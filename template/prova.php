<!DOCTYPE html>
<html lang="en">
<head>

    <!-- start: Meta -->
    <meta charset="utf-8">
    <title>DesignTools</title>

    <!-- end: CSS -->


    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <link id="ie-style" href="css/ie.css" rel="stylesheet">
    <![endif]-->

    <!--[if IE 9]>
        <link id="ie9style" href="css/ie9.css" rel="stylesheet">
    <![endif]-->
 <script src="//rubaxa.github.io/Sortable/Sortable.js"></script>
    <!-- start: Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- end: Favicon -->

    <link href="//rubaxa.github.io/Ply/ply.css" rel="stylesheet" type="text/css"/>
    <link href="//fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css"/>

    <link href="st/app.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<ul id="foo">
    <li data-id="order">order</li>
    <li data-id="save">save</li>
    <li data-id="restore">restore</li>
</ul>
<script type="text/javascript">
var el = document.getElementById('foo');
Sortable.create(el, {
    group: "localStorage-example",
    store: {
        /**
         * Get the order of elements. Called once during initialization.
         * @param   {Sortable}  sortable
         * @returns {Array}
         */
        get: function (sortable) {
            var order = localStorage.getItem(sortable.options.group.name);

            return order ? order.split('|') : [];
            document.write(order);
        },

        /**
         * Save the order of elements. Called onEnd (when the item is dropped).
         * @param {Sortable}  sortable
         */
        set: function (sortable) {
            var order = sortable.toArray();
            localStorage.setItem(sortable.options.group.name, order.join('|'));
            window.location = "prova.php?var="+order;
        }
    }
})
</script>


      

</body>
</html>
