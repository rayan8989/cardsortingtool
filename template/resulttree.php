<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>DesignTools</title>
	<meta name="description" content="Rayan Admin Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->


	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
<style type="text/css">
	.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #A8A8A8; -webkit-border-radius: 20px; -moz-border-radius: 10px; border-radius: 20px; }.datagrid table td, .datagrid table th { padding: 13px 13px; }.datagrid table tbody td { color: #1700E8; border-left: 1px solid #E1EEF4;font-size: 15px;font-weight: normal; }.datagrid table tbody .alt td { background: #F4F4F4; color: #FF0000; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }
</style>
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
<!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        <?php
        $i=1;
        foreach ($lista_task as $key => $valore) {
?>
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Task ok');
        data.addColumn('number', 'Task ko');
        data.addRows([
          ['Success', <?php echo($valore->ok);?>],
          ['Fail', <?php echo($valore->wrong);?>]
        ]);

        // Set chart options
        var options = {
                       'width':300,
                       'height':200};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_<?php print($i); ?>'));
        chart.draw(data, options);
        <?php 
        $i++;
    } ?>
      }
    </script>
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>DesignTools</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">


						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i><?php echo $username; ?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
							<li class="dropdown-menu-title">
 									<span>Impostazioni Account</span>
								</li>
								<li><a href="home.php"><i class="halflings-icon user"></i>Profilo</a></li>
								<li><a href="index.php?comando=logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

		<div class="container-fluid-full">
		<div class="row-fluid">

			<!-- start: Main Menu -->
			<?php
include "sidebar-left.php";
?>
			<!-- end: Main Menu -->

		<!-- start: Messaggio di Errore -->
<?php
$messagge = "";
if ($messagge != "") {

	echo '	<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Well done!</strong>
					<p>' . $messagge . '</p>
				</div>';
}

$messagge_alert = "";
if ($messagge_alert != "") {

	echo '	<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Errore!</strong>
					<p>' . $messagge_alert . '</p>
				</div>';
}

?>
<!-- end: Messaggio di Errore -->

			<!-- start: Content -->
			<div id="content" class="span10">
<div class="row-fluid">
				
				<div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon white th"></i><span class="break"></span>Risultati del tuo Tree Testing</h2>
					</div>
					<div class="box-content">
						<ul class="nav tab-menu nav-tabs" id="myTab">
						<li class="active"><a href="#summary">Sommario</a></li>
									<li><a href="#task">Risultati Task</a></li>
							<li><a href="#path">Paths</a></li>
						
						
						</ul>
						 
						<div id="myTabContent" class="tab-content">
						
							<div class="tab-pane" id="path">
								<h1> Risposte ai Task</h1>
						<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							   <th></th>
							   <th>Path selezionata</th>
								 <th>Nome Task</th>
								  
							
							  </tr>
						  </thead>
						  <tbody>
<?php
$i=0;
//echo json_encode($lista_risposte);
foreach ($lista_risposte as $valore) {
	




	?>		
	<tr>
						<td><?php if($valore[4] == 0){
							echo'<span class="red" style="padding: 1px 12px;">   </span>';
							}else{
							echo'<span class="blue" style="padding: 1px 12px;">   </span>';	
								} ?>
							
						</td>	
							<td>/<?php print($valore[2]); ?>/<?php print($valore[3]); ?></td>
						<td><?php print($valore[1]); ?></td>
				
				
		
								</tr>
								<?php
								$i++;

}



	
?>
						  </tbody>
					  </table>
					</div>

								



							</div>
							<div class="tab-pane" id="task">
								<h1> Statistiche Task </h1>
								</br>		
						
										<?php
										$i=1;
										$label =0;
										foreach ($lista_task as $key => $valore) {
											?>		
	
											<h1><?php print($i); ?>. <?php print($valore->taskn); ?></br>
										<small>Path corretta: /<?php print($valore->categ); ?>/<?php print($valore->sottocat); ?></small></h1>
						<div class="row-fluid sortable ui-sortable">
							<div class="widget span6" ontablet="span6" ondesktop="span6">
					
									<div class="box-content">
						
										 <div id="chart_<?php print($i); ?>"></div>
										 <div class="datagrid">
									 <table>
									 <tbody>
									 <tr>
										<td style="width:200px;">Successo</td><td style="width:200px; text-align: center;"><?php echo($valore->ok);?></td><td style="width:200px; text-align: center;"><?php echo(ceil(($valore->ok/$info_studio[0][5])*100)); ?>%</td></tr>
										<tr class="alt"><td style="width:200px;">Fallimento</td><td style="width:200px; text-align: center;"><?php echo($valore->wrong);?></td><td style="width:200px; text-align: center;"><?php echo(ceil(($valore->wrong/$info_studio[0][5])*100)); ?>%</td></tr>
									</tbody></table>
									</div>
									</div>
							</div>
			
							<div class="widget span6" ontablet="span6" ondesktop="span6">

									<div class="box-content">
										<span>Successo <?php echo(ceil(($valore->ok/$info_studio[0][5])*100)); ?>%</span>
											<div class="progress progress-info progress-striped" style="margin-bottom: 9px; width: 500px !important;">
												<div class="bar" style="width: <?php echo(ceil(($valore->ok/$info_studio[0][5])*100)); ?>%"></div>
											</div>
										<span>Fallimento <?php echo(ceil(($valore->wrong/$info_studio[0][5])*100)); ?>%</span>
											<div class="progress progress-danger progress-striped" style="margin-bottom: 9px; width: 500px !important;">
												<div class="bar" style="width: <?php echo(ceil(($valore->wrong/$info_studio[0][5])*100)); ?>%"></div>
											</div>
					
									</div>
							</div>
			
						</div>
						<p><i>*il totale delle percentuali non è 100 a causa dell'arrontondamento.</i></p>
						<hr>
								<?php
								$i++; } ?>
														
							</div>
												<div class="tab-pane active" id="summary">
<p>Questo studio riguardante il Tree Testing è stato attivato il <?php print(date('d-m-Y', strtotime($info_studio[0][11]))); ?>  <?php if(isset($info_studio[0][12])){ 
								echo"e disattivato il ";
								print(date('d-m-Y', strtotime($info_studio[0][12]))); } ?>.</p>


                            <p><?php echo($info_studio[0][5]); ?> persone hanno partecipato a questo studio.</p>
                          
					
						

								



							</div>

						</div>
					</div>






			



			
			

			



	</div><!--/.fluid-container-->

			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Configura qui titolo</h3>
		</div>
		<div class="modal-body">
			<p>Configura qui</p>
		</div>
		<div class="modal-footer">
	
		</div>
	</div>

	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>

	<div class="clearfix"></div>

	<footer style="background: rgb(8, 8, 8);">

		<p>
			<span style="text-align:left;float:left">&copy; 2017 DesignTools</span>

		</p>

	</footer>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>

		<script src='js/fullcalendar.min.js'></script>

		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>

		<script src="js/jquery.chosen.min.js"></script>

		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>

		<script src="js/jquery.noty.js"></script>

		<script src="js/jquery.elfinder.min.js"></script>

		<script src="js/jquery.raty.min.js"></script>

		<script src="js/jquery.iphone.toggle.js"></script>

		<script src="js/jquery.uploadify-3.1.min.js"></script>

		<script src="js/jquery.gritter.min.js"></script>

		<script src="js/jquery.imagesloaded.js"></script>

		<script src="js/jquery.masonry.min.js"></script>

		<script src="js/jquery.knob.modified.js"></script>

		<script src="js/jquery.sparkline.min.js"></script>

		<script src="js/counter.js"></script>

		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->

</body>
</html>
