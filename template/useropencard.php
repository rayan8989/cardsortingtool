<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>DesignTools</title>
	<meta name="description" content="Rayan Admin Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	<style type="text/css">
	.block__list {

    border: 2px solid #A8A8A8;
    -webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;
   	
       margin-bottom: 10px;
	}
	 .ul{
 	/*margin: 10px !important;*/
 	}   
 	.title {
    color: #fff;
    padding: 3px 10px;
    display: inline-block;
    position: relative;
    background-color: #FF7373;
    z-index: 1000;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    height: 25px;
    margin-left: 10px;
}
	</style>
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
	 <script src="//rubaxa.github.io/Sortable/Sortable.js"></script>
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->

	<link href="//rubaxa.github.io/Ply/ply.css" rel="stylesheet" type="text/css"/>
	<link href="//fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css"/>

	<link href="st/app.css" rel="stylesheet" type="text/css"/>
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>DesignTools</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">


						
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

		<div class="container-fluid-full">
				<div class="row-fluid">



				<div class="span12" style="background-color: white;">
						<div class="page-header">
							  <h1 style="color: black; font-size: 40px; text-align: center;"><small>Benvenuto nello studio:</small> <?php print(strtoupper($info_studio[0][7])); ?></h1>

						  </div>
						<div id="user_istruzioni" class="tooltip-demo well" style="display: none;">
								  <p class="muted" style="margin-bottom: 0; text-align: center;"><?php print($info_studio[0][8]); ?>
								  </p>
								</div>                                  
							  </div>
				<div class="span12" style="background-color: white;">
		
				<a class="quick-button-small span1" onclick="document.getElementById('user_istruzioni').style.display='block';" style="height: 55px; float: right;
    margin-right: 50px;
    margin-bottom: 15px;">
							<i class="halflings-icon info-sign"></i>
							<p>Info</p>
						</a>
						<a class="quick-button-small span1" href="card.php?comando=do&studio=<?php print($_GET["studio"]); ?>&link=<?php print($_GET["link"]); ?>" style="height: 55px; float: right;
    margin-right: 15px;
    margin-bottom: 15px;">
							<i class="halflings-icon repeat"></i>
							<p>Reset</p>
						</a>
		
		
</div>
				</div>
				
					<div class="row-fluid">	
						<div id="done">
						<!-- start: Lista Card -->
						<div class="span3" >
							
							<div style=" background-color: white !important;">
								<div class="layer title" style="margin-left: 10px; ">Lista Card</div>
									<ul id="foo" class="block__list block__list_tags" >
										<?php
											foreach ($_SESSION["card"] as  $valore) {
										 ?>
								   		 <li><?php print(str_replace(' ', '_', $valore)); ?></li>
										<?php } ?>
									</ul>
								</div>

								<button class="btn btn-large btn-primary" style="margin-left: 100px; " onclick="myFunctionDone(2)">Finito</button>					
							</div>
							<!-- end: Lista Card -->

							<!-- start: Content -->
						

						<!-- start: Categorie -->
						<div class="span9" style="margin-top: 20px; margin-left: 20px;">



											<!-- start: NuovaCategoria -->
											<div class="control-group" style="border: 2px solid #A8A8A8; -webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;">
												<h2 class="control-label" style="margin-left:20px;" for="appendedInputButton"><p>Inserisci una nuova categoria</p></h2>
												<div class="controls" style="margin-left:20px;">
												  <div class="input-append">
													<input id="appendedInputButton" name = "appendedInputButton" size="16" type="text" style="border-color: black !important;padding-top: 5px;padding-bottom: 5px;"><button class="btn" type="button" style="
												    padding-top: 5px;
												    padding-bottom: 5px;
												    margin-left: 0px;" Onclick="var addcategoria=$('#appendedInputButton').val(); var ordinamentoll= myFunctionDone(1);  $('#done').load('card.php?comando=do&studio=<?php print($_GET["studio"]); ?>&link=<?php print($_GET["link"]); ?>&addcategoria='+encodeURIComponent(addcategoria)+'&ordinamento='+ordinamentoll);">Crea</button>
												  </div>
												</div>
											  </div>
											<!-- end: NuovaCategoria -->

									<div class="container" style="height: 1520px">

										<script type="text/javascript">
									var el = document.getElementById('foo');


									Sortable.create(el, {
									  group:{
									    name: 'el'
									  },
									  animation: 100
									});
									function myFunctionDone(x) {
										var categorizzazione = {};
											<?php 

										foreach ($_SESSION["categorie"] as  $valore)
											 {?>
												var <?php print($valore); ?> = [];
												var theObject = document.getElementById("<?php print($valore); ?>");
												var lis = theObject.children;
													for(var i = 0; i < lis.length; i++) 
														{
														<?php print($valore); ?>.push(lis[i].innerHTML);
													 	 // alert('<?php print($valore); ?>='+lis[i].innerHTML);
														}

												categorizzazione_tot["<?php print($valore); ?>"]=<?php print($valore); ?>;
										<?php } ?> 
										if (x == 1){
	prova = JSON.stringify(categorizzazione);
 // alert(prova);
 
  	 return prova;
  	}else if (x == 2){
  		prova = JSON.stringify(categorizzazione);
  		window.location = "card.php?comando=fatto&studio=<?php print_r($_GET['studio']); ?>&ordinamento="+prova;
  	}
										}
									function myFunctionOrd() {
										var categorizzazione_tot = {};
											<?php 

										foreach ($_SESSION["categorie"] as  $valore)
											 {?>
												var <?php print($valore); ?>_tot = [];
												var theObject = document.getElementById("<?php print($valore); ?>");
												var lis = theObject.children;
													for(var i = 0; i < lis.length; i++) 
														{
														<?php print($valore); ?>_tot.push(lis[i].innerHTML);
													 	 // alert('<?php print($valore); ?>='+lis[i].innerHTML);
														}
												categorizzazione_tot["<?php print($valore); ?>_tot"]=<?php print($valore); ?>_tot;
										<?php } ?> 
										prova_tot = JSON.stringify(categorizzazione_tot);
									 	// alert(prova);
										  window.location = "card.php?comando=fatto&studio=<?php print_r($_GET['studio']); ?>&ordinamento="+prova_tot;
										}
										</script>
									</div>
	

						</div>	
						<!-- end: Categorie -->
							
					</div>
				</div>

			<!-- end: Content -->
		</div><!--/fluid-row-->

	



	<div class="clearfix"></div>

	<footer style="background: rgb(8, 8, 8);">

		<p>
			<span style="text-align:left;float:left">&copy; 2017 DesignTools</span>

		</p>
	</footer>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
		<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>

		<script src='js/fullcalendar.min.js'></script>

		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
		<script src="js/jquery.flot.js"></script>
		<script src="js/jquery.flot.pie.js"></script>
		<script src="js/jquery.flot.stack.js"></script>
		<script src="js/jquery.flot.resize.min.js"></script>

		<script src="js/jquery.chosen.min.js"></script>

		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>

		<script src="js/jquery.noty.js"></script>

		<script src="js/jquery.elfinder.min.js"></script>

		<script src="js/jquery.raty.min.js"></script>

		<script src="js/jquery.iphone.toggle.js"></script>

		<script src="js/jquery.uploadify-3.1.min.js"></script>

		<script src="js/jquery.gritter.min.js"></script>

		<script src="js/jquery.imagesloaded.js"></script>

		<script src="js/jquery.masonry.min.js"></script>

		<script src="js/jquery.knob.modified.js"></script>

		<script src="js/jquery.sparkline.min.js"></script>

		<script src="js/counter.js"></script>

		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->

</body>
</html>
