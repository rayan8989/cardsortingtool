<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Project E-Learning</title>
	<meta name="description" content="Rayan Admin Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->


	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->

</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>Project Elearning</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">


						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i><?php echo $username; ?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Account Settings</span>
								</li>
								<li><a href="home.php?comando=myvideos"><i class="halflings-icon user"></i> Profile</a></li>
								<li><a href="index.php?comando=logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

		<div class="container-fluid-full">
		<div class="row-fluid">

			<!-- start: Main Menu -->
			<?php
include "sidebar-left.php";
?>
			<!-- end: Main Menu -->

			<!-- start: Messaggio di Errore -->
<?php
$messagge = "";
if ($messagge != "") {

	echo '	<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Well done!</strong>
					<p>' . $messagge . '</p>
				</div>';
}

$messagge_alert = "";
if ($messagge_alert != "") {

	echo '	<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Errore!</strong>
					<p>' . $messagge_alert . '</p>
				</div>';
}

?>
<!-- end: Messaggio di Errore -->
<div id="vot"></div>

			<!-- start: Content -->
			<div id="content" class="span10">
		
			<div class="row-fluid">
<?php

$a = array("yellow", "red", "blue", "green", "pink");

foreach ($lista_stat as $key => $valore){
$random_keys=array_rand($a,1);
		$nomecategoria = ritornacategoria($valore->nome);
?>

<a class="quick-button metro <?php echo $a[$random_keys]; ?> span2">
					<i class="icon-heart-empty"></i>
					<p><?php print strtoupper(($nomecategoria)); ?></p>
					<span class="badge"><?php print($valore->numero); ?></span>
				</a>
<?php
}
?>

				<div class="clearfix"></div>

			</div><!--/row-->
			<br>

<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white user"></i><span class="break"></span>I tuoi video preferiti
 </br></h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								 <th>Titolo</th>
								  <th></th>
								  <th>Guardati</th>
								  <th></th>
								  <th></th>
								    <th></th>
								  <th>Il tuo voto</th>
							
							  </tr>
						  </thead>
						  <tbody>
<?php

foreach ($lista_video_si as $key => $valore) {
	


	?>		
	<tr>
						<td><h2><?php print($valore->nome); ?></h2></td>
				
								<td class="center">
								<iframe id="ytplayer" type="text/html" width="320" height="105"
src="https://www.youtube.com/embed/?listType=playlist&list=<?php print($valore->link); ?>"
frameborder="0" allowfullscreen></iframe></td>
								<td class="center" >
<?php 
      $nome = urlencode($valore->nome); ?>
<div class="input-append">
									<input id="appendedInput<?php print($valore->link); ?>" size="4" placeholder= "<?php print($valore->guardati); ?>" type="number" style="width: 30px;"><span class="add-on">/<?php print($valore->durata); ?></span>
									<button class="btn" type="button" Onclick="var gua<?php print($valore->link); ?>=$('#appendedInput<?php print($valore->link); ?>').val(); $('#vot').load('home.php?comando=myvideos&idvideos=<?php print($valore->id_video); ?>&categoria=<?php print($valore->cat); ?>&nome=<?php print($nome); ?>&link=<?php print($valore->link); ?>&durata=<?php print($valore->durata); ?>&si=<?php print($valore->si); ?>&no=<?php print($valore->no); ?>&guardati='+(gua<?php print($valore->link); ?>));">Aggiorna</button>
								  </div>
</td>
								<td class="center"><i class="halflings-icon thumbs-up"></i><?php print($valore->si); ?></td>
								<td class="center"><i class="halflings-icon thumbs-down"></i><?php print($valore->no); ?></td>
							<td class="center"><iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.youtube.com%2Fembed%2F%3FlistType%3Dplaylist%26list%3D<?php print($valore->link); ?>&layout=button&size=small&mobile_iframe=true&appId=243415942374292&width=78&height=20" width="78" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></td>





								<td class="center">
								<div class="box-content">


<a class="quick-button-small span4" width= "45.982905982905983 !important" Onclick="$('#vot').load('home.php?comando=home&idvideos=<?php print($valore->id_video); ?>&categoria=<?php print($valore->cat); ?>&nome=<?php print($nome); ?>&link=<?php print($valore->link); ?>&durata=<?php print($valore->durata); ?>&si=<?php print($valore->si); ?>&no=<?php print($valore->no); ?>');">
							<i class="glyphicons-icon thumbs_up"></i>
							<p>Like</p>
						
						</a>
 <a class="quick-button-small span4" width= "45.982905982905983" Onclick="$('#vot').load('home.php?comando=home&idvideon=<?php print($valore->id_video); ?>&categoria=<?php print($valore->cat); ?>&nome=<?php print($nome); ?>&link=<?php print($valore->link); ?>&durata=<?php print($valore->durata); ?>&si=<?php print($valore->si); ?>&no=<?php print($valore->no); ?>');">
							<i class="glyphicons-icon thumbs_down"></i>
							<p>Dislike</p>
						

								</div></td>
								</tr>
								<?php




}
	
?>
						  </tbody>
					  </table>
					</div>
				</div>

			</div>


<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white user"></i><span class="break"></span>I video che non ti piacciono
 </br></h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								 <th>Titolo</th>
								  <th></th>
								  <th></th>
								  <th></th>
								  <th></th>
								  <th>Il tuo voto</th>
							
							  </tr>
						  </thead>
						  <tbody>
<?php
$ce = array();
foreach ($lista_video_no as $key => $valore) {
	


	?>		
	<tr>
						<td><h2><?php print($valore->nome); ?></h2></td>
				
								<td class="center">
								<iframe id="ytplayer" type="text/html" width="320" height="105"
src="https://www.youtube.com/embed/?listType=playlist&list=<?php print($valore->link); ?>"
frameborder="0" allowfullscreen></iframe></td>
								<td class="center"><i class="halflings-icon film"></i><?php print($valore->durata); ?>s</td>
								<td class="center"><i class="halflings-icon thumbs-up"></i><?php print($valore->si); ?></td>
								<td class="center"><i class="halflings-icon thumbs-down"></i><?php print($valore->no); ?></td>
							





								<td class="center">
								<div class="box-content">
<?php 
      $nome = urlencode($valore->nome); ?>

<a class="quick-button-small span4" width= "45.982905982905983 !important" Onclick="$('#vot').load('home.php?comando=home&idvideos=<?php print($valore->id_video); ?>&categoria=<?php print($valore->cat); ?>&nome=<?php print($nome); ?>&link=<?php print($valore->link); ?>&durata=<?php print($valore->durata); ?>&si=<?php print($valore->si); ?>&no=<?php print($valore->no); ?>');">
							<i class="glyphicons-icon thumbs_up"></i>
							<p>Like</p>
						
						</a>
 <a class="quick-button-small span4" width= "45.982905982905983" Onclick="$('#vot').load('home.php?comando=home&idvideon=<?php print($valore->id_video); ?>&categoria=<?php print($valore->cat); ?>&nome=<?php print($nome); ?>&link=<?php print($valore->link); ?>&durata=<?php print($valore->durata); ?>&si=<?php print($valore->si); ?>&no=<?php print($valore->no); ?>');">
							<i class="glyphicons-icon thumbs_down"></i>
							<p>Dislike</p>
						

								</div></td>
								</tr>
								<?php




}
	
?>
						  </tbody>
					  </table>
					</div>
				</div>

			</div>


			



			
	



	</div><!--/.fluid-container-->

			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>

	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>

	<div class="clearfix"></div>

	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://themifycloud.com/downloads/janux-free-responsive-admin-dashboard-template/" alt="Bootstrap_Metro_Dashboard">JANUX Responsive Dashboard</a></span>

		</p>

	</footer>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>

		<script src='js/fullcalendar.min.js'></script>

		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>

		<script src="js/jquery.chosen.min.js"></script>

		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>

		<script src="js/jquery.noty.js"></script>

		<script src="js/jquery.elfinder.min.js"></script>

		<script src="js/jquery.raty.min.js"></script>

		<script src="js/jquery.iphone.toggle.js"></script>

		<script src="js/jquery.uploadify-3.1.min.js"></script>

		<script src="js/jquery.gritter.min.js"></script>

		<script src="js/jquery.imagesloaded.js"></script>

		<script src="js/jquery.masonry.min.js"></script>

		<script src="js/jquery.knob.modified.js"></script>

		<script src="js/jquery.sparkline.min.js"></script>

		<script src="js/counter.js"></script>

		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->

</body>
</html>
