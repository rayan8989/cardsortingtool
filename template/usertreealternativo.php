<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<title>DesignTools</title>
	<meta name="description" content="Rayan Admin Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>

	<style type="text/css">

#cssmenu,
#cssmenu ul,
#cssmenu li,
#cssmenu a {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  font-weight: normal;
  text-decoration: none;
  line-height: 1;
  font-family: Arial;
  font-size: 12px;
  position: relative;
}
#cssmenu a {
  line-height: 1.3;
  padding: 6px 0px 6px 16px;
}
#cssmenu > ul > li {
  cursor: pointer;
  background: #000;
}
#cssmenu > ul > li > a {
  font-size: 12px;
  display: block;
  color: #555;
  background: url(Expand.png) no-repeat 0px 12px #FFF;
  line-height:20px;
}
#cssmenu > ul > li > a:hover {
  text-decoration: none;
}
#cssmenu > ul > li.active {
  /*border-bottom: none;*/
}
#cssmenu > ul > li.active > a {
  background: url(collapsed.png) no-repeat 0px 12px #FFF;
  color: #2771ba;
  text-shadow: 0 1px 1px #e6e6e6;
}
#cssmenu > ul > li.has-sub > a:after {
  content: '';
  position: absolute;
  top: 10px;
  right: 10px;
}
#cssmenu > ul > li.has-sub a span{
    color:#2771ba;
    }
#cssmenu > ul > li.has-sub.active > a:after {
  right: 14px;
  top: 12px;
}
/* Sub menu */
#cssmenu ul ul {
  padding: 0;
  display: none;
}
#cssmenu ul ul a {
  background: #fff;
  display: block;
  color: #797979;
  font-size: 12px;
  padding-left:30px;
}
#cssmenu ul li ul li a span{color:#797979 !important;}
#cssmenu ul li ul li a span:hover{color:#7293b4 !important; text-decoration:underline;}
#cssmenu ul ul li:last-child {
  border: none;
}
.addSubSec{list-style:none; padding:10px 0 0 20px; margin:0; background:url(add.png) no-repeat 0 10px; color:#989898;}
.addSub{padding-left:0; border-left:1px solid #d9d9d9;}
.addSub h3{font-size:19px; color:#929292;}

</style>
	<!-- end: CSS -->


	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->



</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>DesignTools</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">


						
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

		<div class="container-fluid-full">
		<div class="row-fluid">
<div class="span12" style="background-color: white;">
<div class="page-header">
							  <h1 style="color: black; font-size: 40px; text-align: center;"><small>Benvenuto nello studio:</small> <?php print(strtoupper($info_studio[0][7])); ?></h1>

						  </div>
						<div class="tooltip-demo well">
								  <p class="muted" style="margin-bottom: 0; text-align: center;"><?php print($info_studio[0][8]); ?>
								  </p>
								</div>                                  
							  </div>
</div>
<div class="container" style="height: 1520px">

<?php

foreach ($lista_task as $keys => $valorea) {
	




	?>		
	<h2>Nome del task: <?php print($valorea->taskn); ?></h2>

 <div id="tasksss">

 <form id="task_<?php print($valorea->idtaskn); ?>">
 <fieldset>
<ul>
<?php
$catprec = "";
$i=0;

foreach ($lista_schema as $key => $valore) {
	
if ($valore->nomec == $catprec){
?>

	 <li><label><input type="radio"  name="task_<?php print($valorea->idtaskn); ?>" value="<?php print($valore->sottoc); ?>"/><span><?php print($valore->sottoc); ?></span></label></li>
<?php

} else 

{
	if ($i == 0){
	?>
	 <li><span><?php print($valore->nomec); ?></span>
                              <ul>
	  <li><label><input type="radio" name="task_<?php print($valorea->idtaskn); ?>" value="<?php print($valore->sottoc); ?>"/><span><?php print($valore->sottoc); ?></span></label></li>
<?php
	 $catprec = $valore->nomec;
	 $i=1;
	 }else {
	 ?>
	  </ul>
                           </li>
	<li><span><?php print($valore->nomec); ?></span>
                              <ul>
	  <li><label><input type="radio" name="task_<?php print($valorea->idtaskn); ?>" value="<?php print($valore->sottoc); ?>"/><span><?php print($valore->sottoc); ?></span></label></li>
	 <?php
 $catprec = $valore->nomec;

	 }
                          
}


}
	?>	
	</li>
	</ul>	
</fieldset>
</form>
                        
                    </div>

<?php } ?>



<button class="btn btn-large btn-primary" style="margin-left: 200px; " onclick="myFunction()">Finito</button>
   






		</div>

	


			

		</div>	
	
	</div>
			




<script type="text/javascript">

function myFunction() {
var utente = [];	// body...
<?php

foreach ($lista_task as $keys => $valorea) {
	




	?>		

utente.push(document.forms.task_<?php print($valorea->idtaskn); ?>.task_<?php print($valorea->idtaskn); ?>.value);
<?php } ?>
task = JSON.stringify(utente);
 window.location = "card.php?comando=fattoto&studio=<?php print_r($_GET['studio']); ?>&link=<?php print_r($_GET['link']); ?>&task="+task;
}
</script>

		<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>

	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>

	<div class="clearfix"></div>

	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://themifycloud.com/downloads/janux-free-responsive-admin-dashboard-template/" alt="Bootstrap_Metro_Dashboard">JANUX Responsive Dashboard</a></span>

		</p>

	</footer>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>

		<script src='js/fullcalendar.min.js'></script>

		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>

		<script src="js/jquery.chosen.min.js"></script>

		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>

		<script src="js/jquery.noty.js"></script>

		<script src="js/jquery.elfinder.min.js"></script>

		<script src="js/jquery.raty.min.js"></script>

		<script src="js/jquery.iphone.toggle.js"></script>

		<script src="js/jquery.uploadify-3.1.min.js"></script>

		<script src="js/jquery.gritter.min.js"></script>

		<script src="js/jquery.imagesloaded.js"></script>

		<script src="js/jquery.masonry.min.js"></script>

		<script src="js/jquery.knob.modified.js"></script>

		<script src="js/jquery.sparkline.min.js"></script>

		<script src="js/counter.js"></script>

		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->

</body>
</html>
