<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>DesignTools</title>
	<meta name="description" content="Rayan Admin Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->

<style type="text/css">
.time{
	font-size: 11px;
    font-weight: bold;
    font-style: italic;

    right: 5px;
}
</style>
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->

</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>DesignTools</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">


						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i><?php echo $username; ?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Impostazioni Account</span>
								</li>
								<li><a href="home.php"><i class="halflings-icon user"></i>Profilo</a></li>
								<li><a href="index.php?comando=logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

		<div class="container-fluid-full">
		<div class="row-fluid">

			<!-- start: Main Menu -->
			<?php
include "sidebar-left.php";
?>
			<!-- end: Main Menu -->

		<!-- start: Messaggio di Errore -->
<?php
$messagge = "";
if ($messagge != "") {

	echo '	<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Well done!</strong>
					<p>' . $messagge . '</p>
				</div>';
}

$messagge_alert = "";
if ($messagge_alert != "") {

	echo '	<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Errore!</strong>
					<p>' . $messagge_alert . '</p>
				</div>';
}

?>
<!-- end: Messaggio di Errore -->

			<!-- start: Content -->
			<div id="content" class="span10">
			<div class="row-fluid sortable" style="text-align: center; border: 2px solid #A8A8A8; -webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;">
<H1 style="margin-right: 140px;">CREA NUOVO STUDIO</H1>
<div class="span5" style="margin-top: 30px; margin-bottom: 30px;">
<!-- <a href="#" class="btn btn-info btn-setting">Card Sorting</a> -->
<a  href="#" class="btn btn-info btn-setting" style="width: 173px !important; height: 103px !important; border-bottom-width: 0px;margin-top: 0px;border-top-width: 0px;padding-top: 0px;padding-bottom: 0px;">
							
							<p style="margin-top:40px !important;">CARD SORTING</p>
						</a>
						</div>
						<div class="span5" style="margin-top: 30px;  margin-bottom: 30px;">
<a  href="home.php?comando=new&type=tree" class="btn" style="width: 173px !important; height: 90px !important; background-color: #5bc0de !important;
    border-color: #5bc0de;">
							
							<p style="margin-top:33px !important;">TREE TESTING</p>
						</a>
						</div>
</div>

<div class="row-fluid sortable" style="margin-top:20px;">
<h1 style="margin-left: 440px;">I TUOI STUDI</h1>
<div class="box-content" style="border: 2px solid #A8A8A8; -webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;">
						<table class="table table-striped table-hover bootstrap-datatable datatable"  >
						  <thead>
							  <tr>
								 <th>Titolo Studio</th>
								  <th>Tipo</th>
								  <th>Data creazione</th>
								    <th>Statistiche</th>
								    <th>Partecipanti</th>
								<th>Stato</th>
								<th></th>
							  </tr>
						  </thead>
						  <tbody>
<?php

foreach ($lista_card_sort as $key => $valore) {
	




	?>		
	<tr class="<?php if($valore->stato == 1){
										echo'success';
									}elseif($valore->stato == 2){
										echo'info';
									}elseif($valore->stato == 3){
										echo'warning';
									} ?>">
						<td><?php print($valore->title); ?></td>
						<td><?php print(Tipo_s($valore->type)); ?></td>
							<td><?php print(date('d-m-Y', strtotime($valore->data))); ?></td>
							<td><?php print(Check_zero($valore->ncards, $tipo = "cards")); ?>
							<?php print(Check_zero($valore->ncategory, $tipo = "categorie")); ?>
							<?php print(Check_zero($valore->ntask, $tipo = "tasks")); ?></td>
							<td><?php print(Check_zero($valore->npartecipant, $tipo = "persone")); ?></td>
								
									<td><?php if($valore->stato == 1){
										echo'<span class="label label-success">In creazione</span>';
									}elseif($valore->stato == 2){
										echo'<span class="label label-inverse">Attivo</span>';
									}elseif($valore->stato == 3){
										echo'<span class="label label-important">Chiuso</span>';
									} ?></td>
									<td class="center">
									<?php if($valore->stato != 1){ ?>
									<a class="btn btn-success" data-toggle="tooltip" title="Risultati" href="home.php?comando=result&tipostudio=<?php print($valore->type); ?>&numerostudio=<?php print($valore->id_study); ?>">
										<i class="halflings-icon white zoom-in"></i>  
									</a>
									<?php } ?>
								<a class="btn btn-info" data-toggle="tooltip" title="Impostazioni" href="home.php?comando=edit&tipostudio=<?php print($valore->type); ?>&numerostudio=<?php print($valore->id_study); ?>">
										<i class="halflings-icon white edit"></i>  
									</a>
										<?php if($valore->stato == 2){ ?>
								<a class="btn btn-danger" data-toggle="tooltip" title="Chiudi studio" href="home.php?comando=edit&tipostudio=<?php print($valore->type); ?>&numerostudio=<?php print($valore->id_study); ?>&active=no">
										<i class="halflings-icon white trash"></i> 
									</a>	<?php } ?></td>
								</tr>
								<?php

}



	
?>
						  </tbody>
					  </table>
					</div>







			
</div>

<div id="vot"></div>
			
			

			



	</div><!--/.fluid-container-->

			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Card Sorting</h3>
		</div>
		<div class="modal-body">
			<p>Seleziona il tipo di Card Sorting</p>
		</div>
		<div class="modal-footer">
			<a href="home.php?comando=new&type=open" class="quick-button"   style= " float: left;
padding: 0px 10px 10px 10px; margin-left: 50px;" >
							<i class="glyphicons-icon inbox_plus"></i>
							<p>Nuovo Card Sort Aperto</p>
						</a>
						<a  href="home.php?comando=new&type=closed" class="quick-button" style= " float: right;
padding: 0px 10px 10px 10px; margin-right: 50px;" >
							<i class="glyphicons-icon inbox_lock"></i>
							<p>Nuovo Card Sort Chiuso</p>
						</a>
		</div>
	</div>

	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>

	<div class="clearfix"></div>

	<footer style="background: rgb(8, 8, 8);">

		<p>
			<span style="text-align:left;float:left">&copy; 2017 DesignTools</span>

		</p>

	</footer>

	<!-- start: JavaScript-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>

		<script src='js/fullcalendar.min.js'></script>

			<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>

		<script src="js/jquery.chosen.min.js"></script>

		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>

		<script src="js/jquery.noty.js"></script>

		<script src="js/jquery.elfinder.min.js"></script>

		<script src="js/jquery.raty.min.js"></script>

		<script src="js/jquery.iphone.toggle.js"></script>

		<script src="js/jquery.uploadify-3.1.min.js"></script>

		<script src="js/jquery.gritter.min.js"></script>

		<script src="js/jquery.imagesloaded.js"></script>

		<script src="js/jquery.masonry.min.js"></script>

		<script src="js/jquery.knob.modified.js"></script>

		<script src="js/jquery.sparkline.min.js"></script>

		<script src="js/counter.js"></script>

		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
<script type="text/javascript">
	$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})</script>
</body>
</html>
