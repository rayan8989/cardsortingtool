<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>DesignTools</title>
	<meta name="description" content="Rayan Admin Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>

	<style type="text/css">

#cssmenu,
#cssmenu ul,
#cssmenu li,
#cssmenu a {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  font-weight: normal;
  text-decoration: none;
  line-height: 1;
  font-family: Arial;
  font-size: 12px;
  position: relative;
}
#cssmenu a {
  line-height: 1.3;
  padding: 6px 0px 6px 16px;
}
#cssmenu > ul > li {
  cursor: pointer;
  background: #000;
}
#cssmenu > ul > li > a {
  font-size: 12px;
  display: block;
  color: #555;
  background: url(Expand.png) no-repeat 0px 12px #FFF;
  line-height:20px;
}
#cssmenu > ul > li > a:hover {
  text-decoration: none;
}
#cssmenu > ul > li.active {
  /*border-bottom: none;*/
}
#cssmenu > ul > li.active > a {
  background: url(collapsed.png) no-repeat 0px 12px #FFF;
  color: #2771ba;
  text-shadow: 0 1px 1px #e6e6e6;
}
#cssmenu > ul > li.has-sub > a:after {
  content: '';
  position: absolute;
  top: 10px;
  right: 10px;
}
#cssmenu > ul > li.has-sub a span{
    color:#2771ba;
    }
#cssmenu > ul > li.has-sub.active > a:after {
  right: 14px;
  top: 12px;
}
/* Sub menu */
#cssmenu ul ul {
  padding: 0;
  display: none;
}
#cssmenu ul ul a {
  background: #fff;
  display: block;
  color: #797979;
  font-size: 12px;
  padding-left:30px;
}
#cssmenu ul li ul li a span{color:#797979 !important;}
#cssmenu ul li ul li a span:hover{color:#7293b4 !important; text-decoration:underline;}
#cssmenu ul ul li:last-child {
  border: none;
}
.addSubSec{list-style:none; padding:10px 0 0 20px; margin:0; background:url(add.png) no-repeat 0 10px; color:#989898;}
.addSub{padding-left:0; border-left:1px solid #d9d9d9;}
.addSub h3{font-size:19px; color:#929292;}

</style>
	<!-- end: CSS -->


	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->

</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>DesignTools</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">


						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i><?php echo $username; ?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Impostazioni Account</span>
								</li>
								<li><a href="home.php"><i class="halflings-icon user"></i>Profilo</a></li>
								<li><a href="index.php?comando=logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

		<div class="container-fluid-full">
		<div class="row-fluid">

			<!-- start: Main Menu -->
			<?php
include "sidebar-left.php";
?>
			<!-- end: Main Menu -->

		<!-- start: Messaggio di Errore -->
<?php
$messagge = "";
if ($messagge != "") {

	echo '	<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Well done!</strong>
					<p>' . $messagge . '</p>
				</div>';
}

$messagge_alert = "";
if ($messagge_alert != "") {

	echo '	<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Errore!</strong>
					<p>' . $messagge_alert . '</p>
				</div>';
}

?>
<!-- end: Messaggio di Errore -->

			<!-- start: Content -->
			<div id="content" class="span10">
<div class="row-fluid">
				
				<div class="box span12">
					<div class="box-header">
						<h2><i class="halflings-icon white th"></i><span class="break"></span>Configura il tuo Tree Testing</h2>
					</div>
					<div class="box-content">
						<ul class="nav tab-menu nav-tabs" id="myTab">
							<li class="active"><a href="#impostazioni">Impostazioni Studio</a></li>
							<li><a href="#schema">Inserisci Schema</a></li>
							<li><a href="#task">Inserisci Task</a></li>
							<li><a href="#info">Partecipanti</a></li>
							
						</ul>
						 
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane" id="info">
								<?php 
									if($info_studio[0][10] == 1){ ?>

							<p>Per generare il link per la condivisione dello studio, cliccare sul pulsante Attiva.</p>
							<p>Attenzione! Non sarà più possibile modificare lo studio dopo aver attivato lo studio.</p>
							<a style="margin-bottom: 0px;" href="home.php?comando=edit&tipostudio=<?php print($_GET["tipostudio"]); ?>&numerostudio=<?php echo($_GET['numerostudio']); ?>&active=yes" class="quick-button span2" style="width: 173px !important; height: 103px !important;">
							<img style="width: 40px; height: 40px;" src="img/icon-activate.png"></img>
							<p>ATTIVA</p>
							</a>

										
						<?php
								} else {
									?>
							<p>
						Il link per condividere lo studio è il seguente: <?php echo ROOT .  DS . 'card.php?comando=do&studio='.$_GET['numerostudio'].'&link='.$info_studio[0][6]; ?>
						</p>
							


							<?php	} ?>
								
							<div class="actions" style="float: right; clear:both;">
					
									<button tabindex="3" type="button" class="btn btn-success" onclick="location.href='home.php'">Indietro</button>
									
								</div>
							</div>
								<div class="tab-pane" id="schema">

							<div class="control-group">
								<label class="control-label" for="appendedInputButton">Inserisci una nuova categoria</label>
								<div class="controls">
								  <div class="input-append">
									<input id="appendedInputButton" name = "appendedInputButton" size="16" type="text">
									<?php 
									if($info_studio[0][10] == 1){ ?>
									<button class="btn" type="button" Onclick="var categoria=$('#appendedInputButton').val(); $('#categorieschema').load('home.php?comando=edit&tipostudio=<?php print($_GET["tipostudio"]); ?>&numerostudio=<?php print($_GET["numerostudio"]); ?>&categoria='+encodeURIComponent(categoria));">Inserisci</button>
									<?php } ?>
								  </div>
								</div>
							  </div>
<div id="categorieschema">
							  <div class="control-group">
								<label class="control-label" for="appendedInputButtonr">Inserisci la sottocategoria e seleziona la categoria</label>
								<div class="controls">
								  <div class="input-append">
									<input id="appendedInputButtonr" name = "appendedInputButtonr" size="16" type="text">
									
									  <select id="provinciac" name="provinciac">
        <?php
        // visualizzo le categorie
        foreach ($lista_categorie as $key => $valore)
         {       ?>
        <option value="<?php print($valore->cat); ?>"><?php print($valore->cat); ?></option>
        <?php    
        } ?>
        </select>
						<?php 
									if($info_studio[0][10] == 1){ ?>
									<button class="btn" type="button" Onclick="var provc=$('#provinciac').val(); var sottocat=$('#appendedInputButtonr').val(); $('#schemaaggiornato').load('home.php?comando=edit&tipostudio=<?php print($_GET["tipostudio"]); ?>&numerostudio=<?php print($_GET["numerostudio"]); ?>&sottocat='+encodeURIComponent(sottocat)+'&cate='+encodeURIComponent(provc));">Inserisci</button>
										<?php } ?>
								  </div>
								</div>
							  </div>
</div>
							 
<div id="cssmenu" style="text-align: center; margin-bottom: 30px; margin-top: 30px; border: 2px solid #A8A8A8; -webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;">
<h2>Il tuo attuale schema</h2>
<ul>
<?php
$catprec = "";
$i=0;

foreach ($lista_schema as $key => $valore) {
	
if ($valore->nomec == $catprec){
?>
	 <li class="even"><a href="#"><span><?php print($valore->sottoc); ?></span></a></li>
<?php

} else 

{
	if ($i == 0){
	?>
	 <li class="has-sub"><a href="#"><span><?php print($valore->nomec); ?></span></a>
                              <ul>
	 <li class="even"><a href="#"><span><?php print($valore->sottoc); ?></span></a></li>
<?php
	 $catprec = $valore->nomec;
	 $i=1;
	 }else {
	 ?>
	  </ul>
                           </li>
	<li class="has-sub"><a href="#"><span><?php print($valore->nomec); ?></span></a>
                              <ul>
	 <li class="even"><a href="#"><span><?php print($valore->sottoc); ?></span></a></li>
	 <?php
 $catprec = $valore->nomec;

	 }
                          
}


}
	?>	
	</ul>	

                        
                    </div>
<div id="schemaaggiornato">
<h2>Tabella del tuo attuale schema</h2>
								<div class="box-content" style="border: 2px solid #A8A8A8; -webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								 <th>Categoria</th>
								 <th>Sottocategoria</th>
								  <th>Elimina</th>
							
							  </tr>
						  </thead>
						  <tbody>
<?php

foreach ($lista_schema as $key => $valore) {
	




	?>		
	<tr>
						<td><?php print($valore->nomec); ?></td>
						<td><?php print($valore->sottoc); ?></td>
				
								
							





								<td class="center">
								<?php 
									if($info_studio[0][10] == 1){ ?>
								<a class="btn btn-danger" Onclick="$('#schemaaggiornato').load('home.php?comando=edit&tipostudio=<?php print($_GET["tipostudio"]); ?>&numerostudio=<?php print($_GET["numerostudio"]); ?>&sottocatelimina=<?php print($valore->sottoc); ?>&categoriaelimina=<?php print($valore->nomec); ?>');">
										<i class="halflings-icon white trash"></i> 
									</a>
									<?php } ?></td>
								</tr>
								<?php

}



	
?>
						  </tbody>
					  </table>
					</div>
					<div class="actions" style="float: right; margin: 10px;">
					
									<button tabindex="3" type="button" class="btn btn-success" onclick="location.href='home.php'">Indietro</button>
									
								</div>

</div>










							</div>
							<div class="tab-pane" id="task">

							<div class="control-group">
								<label class="control-label" for="appendedInputButtond">Inserisci una nuovo task e il percorso corretto</label>
								<div class="controls">
								  <div class="input-append">
									<input id="appendedInputButtond" name = "appendedInputButtond" size="16" type="text" style="width: 850px;">

											 <select id="provinciaco" name="provinciaco">
        <?php
        // visualizzo le regioni
        foreach ($lista_schema as $key => $valore)
         {       ?>
        <option value="<?php print($valore->id); ?>"><?php print($valore->nomec); ?>/<?php print($valore->sottoc); ?>  </option>
        <?php    
        } ?>
        </select>
	<?php 
									if($info_studio[0][10] == 1){ ?>

									<button class="btn" type="button" Onclick="var task=$('#appendedInputButtond').val(); var percorso=$('#provinciaco').val(); $('#listatask').load('home.php?comando=edit&tipostudio=<?php print($_GET["tipostudio"]); ?>&numerostudio=<?php print($_GET["numerostudio"]); ?>&task='+encodeURIComponent(task)+'&percorso='+encodeURIComponent(percorso));">Inserisci</button>
									<?php } ?>
								  </div>
								</div>
							  </div>

<div id="listatask">
								<div class="box-content" style="border: 2px solid #A8A8A8; -webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								 <th>Descrizione task</th>
								 <th>Percorso corretto</th>
								  <th>Elimina</th>
							
							  </tr>
						  </thead>
						  <tbody>
<?php

foreach ($lista_task as $key => $valore) {
	




	?>		
	<tr>
						<td><?php print($valore->taskn); ?></td>
				
								<td><?php print($valore->categ); ?>/<?php print($valore->sottocat); ?></td>
							





								<td class="center">
								<?php if($info_studio[0][10] == 1){ ?>
								<a class="btn btn-danger"  Onclick="$('#listatask').load('home.php?comando=edit&tipostudio=<?php print($_GET["tipostudio"]); ?>&numerostudio=<?php print($_GET["numerostudio"]); ?>&taskelimina=<?php print($valore->idtaskn); ?>');">
										<i class="halflings-icon white trash"></i> 
									</a>
									<?php } ?></td>
								</tr>
								<?php

}



	
?>
						  </tbody>
					  </table>
					</div>

					<div class="actions" style="float: right; margin: 10px;">
					
									<button tabindex="3" type="button" class="btn btn-success" onclick="location.href='home.php'">Indietro</button>
									
								</div>
								</div>
							</div>
								<div class="tab-pane active" id="impostazioni">
							
					
					
						<form class="form-horizontal">
						  <fieldset>
							


						<div class="control-group">
								<label class="control-label" for="appendedInputButtont">Titolo</label>
								<div class="controls">
								  <div class="input-append">
								  <?php 
									if($info_studio[0][10] == 1){ ?>
											<input id="appendedInputButtont" name = "appendedInputButtont" size="16" type="text" style="width: 800px;" value="<?php print($info_studio[0][7]); ?>">
											<?php }else{ ?>
								  <input class="input-xlarge disabled" id="disabledInput" style="width: 800px;" type="text" placeholder="<?php print($info_studio[0][7]); ?>" disabled="">
							<?php } ?>
								  </div>
								</div>
							  </div>
						
						<div class="control-group">
								<label class="control-label" for="appendedInputButtontt">Istruzioni per i partecipanti</label>
								<div class="controls">
								  <div class="input-append">
								  <?php 
									if($info_studio[0][10] == 1){ ?>

									<textarea style="width: 800px;" id="appendedInputButtontt" name = "appendedInputButtontt" rows="5"  placeholder="<?php print($info_studio[0][8]); ?>"></textarea>
									<?php }else{ ?>
									<textarea style="width: 800px;" id="appendedInputButtontt" name = "appendedInputButtontt" rows="5"  placeholder="<?php print($info_studio[0][8]); ?>" disabled=""></textarea>
								 
							<?php } ?>
								  </div>
								</div>
							  </div>

						
							
						  </fieldset>
						</form>   

					<div class="actions" style="float: right;">
					 <?php 
									if($info_studio[0][10] == 1){ ?>
									<button tabindex="3" type="button" class="btn btn-success" Onclick="var titolo=$('#appendedInputButtont').val(); var istruzioni=$('#appendedInputButtontt').val(); $('#done').load('home.php?comando=edit&tipostudio=<?php print($_GET["tipostudio"]); ?>&numerostudio=<?php print($_GET["numerostudio"]); ?>&titolo='+encodeURIComponent(titolo)+'&istruzioni='+encodeURIComponent(istruzioni));">Salva</button>
									<?php } ?>
									<button tabindex="3" type="button" class="btn btn-success" onclick="location.href='home.php'">Indietro</button>
									
								</div>
				
								
							</div>

			


						</div>
					</div>
				</div><!--/span-->
			
			</div>


			


<div id="done"></div>
			
			

			



	</div><!--/.fluid-container-->

			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>

	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>

	<div class="clearfix"></div>

	<footer style="background: rgb(8, 8, 8);">
<p>
			<span style="text-align:left;float:left">&copy; 2017 DesignTools</span>

		</p>
<script type="text/javascript">
$( document ).ready(function() {
$('#cssmenu ul ul li:odd').addClass('odd');
$('#cssmenu ul ul li:even').addClass('even');
$('#cssmenu > ul > li > a').click(function() {
  $('#cssmenu li').removeClass('active');
  $(this).closest('li').addClass('active'); 
  var checkElement = $(this).next();
  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
    $(this).closest('li').removeClass('active');
    checkElement.slideUp('normal');
  }
  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
    $('#cssmenu ul ul:visible').slideUp('normal');
    checkElement.slideDown('normal');
  }
  if($(this).closest('li').find('ul').children().length == 0) {
    return true;
  } else {
    return false;   
  }     
});
});
</script>
	</footer>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>

		<script src='js/fullcalendar.min.js'></script>

		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>

		<script src="js/jquery.chosen.min.js"></script>

		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>

		<script src="js/jquery.noty.js"></script>

		<script src="js/jquery.elfinder.min.js"></script>

		<script src="js/jquery.raty.min.js"></script>

		<script src="js/jquery.iphone.toggle.js"></script>

		<script src="js/jquery.uploadify-3.1.min.js"></script>

		<script src="js/jquery.gritter.min.js"></script>

		<script src="js/jquery.imagesloaded.js"></script>

		<script src="js/jquery.masonry.min.js"></script>

		<script src="js/jquery.knob.modified.js"></script>

		<script src="js/jquery.sparkline.min.js"></script>

		<script src="js/counter.js"></script>

		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->

</body>
</html>
