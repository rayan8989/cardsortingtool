<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>DesignTools</title>
	<meta name="description" content="Rayan Admin Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<style type="text/css">
.block__list {

    border: 2px solid #A8A8A8;
    -webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;
   	
       margin-bottom: 10px;
	}
	 .ul{
 	/*margin: 10px !important;*/
 	}   
 	.title {
    color: #fff;
    padding: 3px 10px;
    display: inline-block;
    position: relative;
    background-color: #FF7373;
    z-index: 1000;
    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    border-radius: 15px;
    height: 25px;
    margin-left: 10px;
}
</style>
	<!-- end: CSS -->


	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
 <script src="//rubaxa.github.io/Sortable/Sortable.js"></script>
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->

	<link href="//rubaxa.github.io/Ply/ply.css" rel="stylesheet" type="text/css"/>
	<link href="//fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css"/>

	<link href="st/app.css" rel="stylesheet" type="text/css"/>
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>DesignTools</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">


						
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

				<div class="container-fluid-full">
		<div class="row-fluid">
<div class="span12" style="background-color: white;">
<div class="page-header">
							  <h1 style="color: black; font-size: 40px; text-align: center;"><small>Benvenuto nello studio:</small> <?php print(strtoupper($info_studio[0][7])); ?></h1>

						  </div>
						<div id="user_istruzioni" class="tooltip-demo well" style="display: none;">
								  <p class="muted" style="margin-bottom: 0; text-align: center;"><?php print($info_studio[0][8]); ?>
								  </p>
								</div>                                  
							  </div>
		<div class="span12" style="background-color: white;">
		
				<a class="quick-button-small span1" onclick="document.getElementById('user_istruzioni').style.display='block';" style="height: 55px; float: right;
    margin-right: 50px;
    margin-bottom: 15px;">
							<i class="halflings-icon info-sign"></i>
							<p>Info</p>
						</a>
						<a class="quick-button-small span1" href="card.php?comando=do&studio=<?php print($_GET["studio"]); ?>&link=<?php print($_GET["link"]); ?>" style="height: 55px; float: right;
    margin-right: 15px;
    margin-bottom: 15px;">
							<i class="halflings-icon repeat"></i>
							<p>Reset</p>
						</a>
		
		
</div>
</div>
<div class="row-fluid">			<!-- start: Lista Card -->
<div class="span3" >
			
			<div style=" background-color: white !important;">
				<div class="layer title" style="margin-left: 10px; ">Lista Card</div>
					<ul id="foo" class="block__list block__list_tags" >
						<?php
							foreach ($lista_card as $key => $valore) {
						 ?>
				   		 <li><?php print($valore->nome); ?></li>
						<?php } ?>
					</ul>
				</div>

				<button class="btn btn-large btn-primary" style="margin-left: 100px; " onclick="myFunction()">Finito</button>					
			</div>

			<!-- start: Content -->
			<div class="span9" style="margin-top: 20px; margin-left: 20px;">

	<div class="container" style="height: 1520px">

		<?php

foreach ($lista_categorie as $key => $valore) {
	




	?>	
			<div class="span3" >
			<div class="layer title"><?php print($valore->cat); ?></div>
			<ul id="<?php print(escapeLine($valore->cat)); ?>" class="block__list block__list_words">
				
			</ul>
		</div>
		
		<?php } ?>
		




<script type="text/javascript">
var card = document.getElementById('foo');
<?php
foreach ($lista_categorie as $key => $valore) {
	?>
var <?php print(escapeLine($valore->cat)); ?> = document.getElementById('<?php print(escapeLine($valore->cat)); ?>');
<?php
}

	?>		

Sortable.create(card, {
  group:{
    name: 'card'
  },
  animation: 100
});

<?php 

foreach ($lista_categorie as $key => $valore) {

 ?>

Sortable.create(<?php print(escapeLine($valore->cat)); ?>, {
  group: {
    name: 'card'
  },
  animation: 100
});
<?php } ?>

function myFunction() {
	var categorizzazione = {};
<?php 

foreach ($lista_categorie as $key => $valore) {

 ?>
var <?php print(escapeLine($valore->cat)); ?> = [];
var theObject = document.getElementById("<?php print(escapeLine($valore->cat)); ?>");
var lis = theObject.children;
for(var i = 0; i < lis.length; i++) {

	<?php print(escapeLine($valore->cat)); ?>.push(lis[i].innerHTML);
  // alert('<?php print(strtolower($valore->cat)); ?>='+lis[i].innerHTML);
}
categorizzazione["<?php print(escapeLine($valore->cat)); ?>"]=<?php print(escapeLine($valore->cat)); ?>;
<?php } ?> 
prova = JSON.stringify(categorizzazione);
  //alert(prova);
  window.location = "card.php?comando=fatto&studio=<?php print_r($_GET['studio']); ?>&link=<?php print_r($_GET['link']); ?>&ordinamento="+prova;
}
</script>

		<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>

	<div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<ul class="list-inline item-details">
				<li><a href="http://themifycloud.com">Admin templates</a></li>
				<li><a href="http://themescloud.org">Bootstrap themes</a></li>
			</ul>
		</div>
	</div>

	<div class="clearfix"></div>

	<footer>

	<p>
			<span style="text-align:left;float:left">&copy; 2017 DesignTools</span>

		</p>

	</footer>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>

		<script src='js/fullcalendar.min.js'></script>

		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>

		<script src="js/jquery.chosen.min.js"></script>

		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>

		<script src="js/jquery.noty.js"></script>

		<script src="js/jquery.elfinder.min.js"></script>

		<script src="js/jquery.raty.min.js"></script>

		<script src="js/jquery.iphone.toggle.js"></script>

		<script src="js/jquery.uploadify-3.1.min.js"></script>

		<script src="js/jquery.gritter.min.js"></script>

		<script src="js/jquery.imagesloaded.js"></script>

		<script src="js/jquery.masonry.min.js"></script>

		<script src="js/jquery.knob.modified.js"></script>

		<script src="js/jquery.sparkline.min.js"></script>

		<script src="js/counter.js"></script>

		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->

</body>
</html>
