<!DOCTYPE html>
<html lang="en">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>DesignTools</title>
	<meta name="description" content="Rayan Admin Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->


	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->

	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->

</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>DesignTools</span></a>

				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">


						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i><?php echo $username; ?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
 									<span>Impostazioni Account</span>
								</li>
								<li><a href="home.php"><i class="halflings-icon user"></i>Profilo</a></li>
								<li><a href="index.php?comando=logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->

		<div class="container-fluid-full">
		<div class="row-fluid">

			<!-- start: Main Menu -->
			<?php
include "sidebar-left.php";
?>
			<!-- end: Main Menu -->

		<!-- start: Messaggio di Errore -->
<?php
$messagge = "";
if ($messagge != "") {

	echo '	<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Well done!</strong>
					<p>' . $messagge . '</p>
				</div>';
}

$messagge_alert = "";
if ($messagge_alert != "") {

	echo '	<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Errore!</strong>
					<p>' . $messagge_alert . '</p>
				</div>';
}

?>
<!-- end: Messaggio di Errore -->

			<!-- start: Content -->
			<div id="content" class="span10">
			<div class="row-fluid">
				
				<div class="box span12">
	<div class="box-header">
						<h2><i class="halflings-icon white th"></i><span class="break"></span>Risultati del tuo Card Sorting</h2>
					</div>
					<div class="box-content">
						<ul class="nav tab-menu nav-tabs" id="myTab">
							<li class="active"><a href="#summary">Sommario</a></li>
							<li><a href="#card">Card</a></li>
							<li><a href="#cat">Categorie</a></li>
							<li><a href="#tab">Tabella</a></li>
							
							
						
						</ul>
						 
						<div id="myTabContent" class="tab-content">
						<div class="tab-pane" id="tab">
							<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  
								 <th>Card</th>
								 <?php
							//echo json_encode($lista_card_categorie);
							
							foreach ($lista_categorie as $key => $valore) {


								 echo' <th>'.$valore->cat.'</th>';
								
								    } ?>
							
							  </tr>
						  </thead>
						  <tbody>
						   
							<?php
							//echo'<tr>';
							foreach ($tabella_totale as $valore){
								//echo $key;
								echo'<tr>';
								$i=0;
								foreach($valore[0] as $chiave => $val){
									if($i == 0){
										$i++;
									}else{
									//echo $tabella_totale[$key][0]['bari'];
										 if ((is_numeric($val)) && ($val > 0)){
										 	echo '<td><span class="green" style="padding: 1px 12px;">'.$val.'</span></td>';
										 }elseif ((is_numeric($val)) && ($val == 0)){
								echo '<td class="center"><span class="grey" style="padding: 1px 12px;">'.$val.'</span></td>';
							}else{
								echo '<td class="center"><b>'.$val.'</b></td>';
							}
									}
									//echo $chiave."-".$val;
								}
								echo"</tr>";
							}
							//print_r($tabella_totale);
						

				?>
				</tbody>
					  </table>
					</div>
								
							</div>
							<div class="tab-pane" id="cat">
							<h1>Le categorie</h1>
							<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								 <th>Nome Categoria</th>
								  <th>N Card associate</th>
								   <th>Card associate/Frequenza</th>
								    
							
							  </tr>
						  </thead>
						  <tbody>

							<?php
							//echo json_encode($lista_card_categorie);
							
							foreach ($lista_card_categorie as $key => $valore) {

								echo'<tr><td class="center">'.$lista_card_categorie[$key][0].'</td><td class="center">'.$lista_card_categorie[$key][1].'</td><td class="center">';
						

							

								//$or = json_decode($lista_card_categorie[$key][2], true);
								 $i=0;
								 echo'<table>';
								foreach ($lista_card_categorie[$key][2] as  $val){
									
									if($i == 1){
										 echo'<td style="
    padding-left: 20px;
">';

										echo'<span class="green" style="padding: 1px 12px;">'.($val).'</span>';
										echo'</td>';
										echo'</tr>';
										
										$i=0;
									}else{
										echo '<tr>';
										 echo'<td style="
    padding-left: 20px;
">';
										echo($val);
									echo'</td>';
										$i++;
									}
									//echo($val);
									//echo"</br>";
									//echo $lista_card_categorie[$key][2][$val];
									//echo json_encode($lista_card_categorie[$key][2])."</br>";
								}
								echo'</table>';
								echo'</td>';	
								
							}
							echo'</tr>';		
							?>	
</tbody>
					  </table>
					</div>
							</div>
							<div class="tab-pane" id="card">
							<h1>Le Card</h1>
							<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								 <th>Nome Card</th>
								  <th>Categorie associate/Frequenza</th>
								   <th>Frequenza</th>
							
							  </tr>
						  </thead>
						  <tbody>
<?php
//print_r($lista_risultati);
$card = "";
foreach ($lista_risultati as $key => $valore) {
	




	?>		
	<tr>
						<td>



						<?php 
						if(strcmp($card, $valore->nome) == 0){
							echo $valore->nome;
							//echo "uguali";

						}else{
				
						echo($valore->nome);
						$card = $valore->nome;
					}
						 ?></td>
				
								
							





								<td class="center">
								<?php print($valore->catnome); ?>
								</td>

								<td class="center">
								<span class="green" style="padding: 1px 12px;"><?php print($valore->numero); ?></span>
								</td>
								</tr>
								<?php

}



	
?>
						  </tbody>
					  </table>
					</div>



							</div>
							<div class="tab-pane active" id="summary">
							
							<p>Questo studio riguardante il card sorting aperto è stato attivato il <?php print(date('d-m-Y', strtotime($info_studio[0][11]))); ?>  <?php if(isset($info_studio[0][12])){ 
								echo"e disattivato il ";
								print(date('d-m-Y', strtotime($info_studio[0][12]))); } ?>.</p>

                            <p><?php echo($info_studio[0][5]); ?> persone hanno partecipato a questo studio.</p>
                            <p><?php echo($info_studio[0][5]); ?> persone hanno partecipato e hanno suddiviso le <?php echo($info_studio[0][2]); ?> cards in un totale di <?php echo($categorie_totali); ?> diverse categorie.</p>
					
						

				
				
								
							</div>

						</div>
					</div>



	</div><!--/.fluid-container-->

			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

	</div>
</div>

	<div class="clearfix"></div>

	<footer style="background: rgb(8, 8, 8);">

		<p>
			<span style="text-align:left;float:left">&copy; 2017 DesignTools</span>

		</p>

	</footer>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>

		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

		<script src="js/jquery.ui.touch-punch.js"></script>

		<script src="js/modernizr.js"></script>

		<script src="js/bootstrap.min.js"></script>

		<script src="js/jquery.cookie.js"></script>

		<script src='js/fullcalendar.min.js'></script>

		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>

		<script src="js/jquery.chosen.min.js"></script>

		<script src="js/jquery.uniform.min.js"></script>

		<script src="js/jquery.cleditor.min.js"></script>

		<script src="js/jquery.noty.js"></script>

		<script src="js/jquery.elfinder.min.js"></script>

		<script src="js/jquery.raty.min.js"></script>

		<script src="js/jquery.iphone.toggle.js"></script>

		<script src="js/jquery.uploadify-3.1.min.js"></script>

		<script src="js/jquery.gritter.min.js"></script>

		<script src="js/jquery.imagesloaded.js"></script>

		<script src="js/jquery.masonry.min.js"></script>

		<script src="js/jquery.knob.modified.js"></script>

		<script src="js/jquery.sparkline.min.js"></script>

		<script src="js/counter.js"></script>

		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->

</body>
</html>
