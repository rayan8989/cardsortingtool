  <?php
//Classi per la connessione al db
require_once ROOT . DS . "config" . DS . "db.php";
//Classi per gestire i dati
include_once ROOT . DS . "config" . DS . "function.php";
include_once ROOT . DS . "model" . DS . "Category.php";

class Modello {

	//recupera i dati dell'utente
	public function recupera_login($email, $password) {
		$login_data = new Db();
		$query = "SELECT * FROM utenti WHERE username = '$email' AND password = '$password'";
		$result = $login_data->query($query);

		$numrows = mysqli_num_rows($result);
		//echo $numrows;
		if ($numrows == 1) {
			while ($com = mysqli_fetch_assoc($result)) {
				$login[] = [$com['id_user'], $com['username']];
			}
			//ritorna il login
			return $login;
		} else {
			$login = null;
			return $login;
		}
	}

	//verifica se esiste già un utente 
public function recupera_user($username) {
		$login_data = new Db();
		$query = "SELECT * FROM utenti WHERE username = '$username'";
		$result = $login_data->query($query);

		$numrows = mysqli_num_rows($result);
		//echo $numrows;
			
		
			return $numrows;
		
	}

   //registra nuovo utente
	public function registra_utente($username, $password) {

		$lista = new Db();
		$query = "INSERT INTO utenti (username, password) VALUES ('$username', '$password')";
		$result = $lista->query($query);
		
		return $result;
	}

   //aggiungi nuovo studio
public function aggiungi_studio($type, $id_utente) {

		$lista = new Db();
		//CREARE VARIABILE TEMPO CON TIMESTAMP CORRENTE
		$link = rand();
		$query = "INSERT INTO studies (type, link, id_user, data_creazione, state) VALUES ('$type', '$link', '$id_utente', now(), 1)";
		$result = $lista->query($query);
		//NEL WHERE INSERIRE IL TIMESTAMP
		$listanumero = new Db();
		$querynumero = "SELECT id_studies FROM studies WHERE type = '$type' AND id_user = $id_utente ORDER BY data_creazione DESC LIMIT 1";
		$resultnumero = $listanumero->query($querynumero);
			while ($risultati = mysqli_fetch_assoc($resultnumero)){

				$numero = $risultati['id_studies'];
			}

		$querydue = "CREATE TABLE studies_openclosed_id_$numero (
		id_card serial not null,
		card_name text,
		category_name text,
		n_occorrenza integer DEFAULT 0,

		CONSTRAINT kcard_$numero PRIMARY KEY (id_card));";
		$resultdue = $lista->query($querydue);

		if($type == 'closed' || $type == 'open'){
			$querytre = "CREATE TABLE studies_category_$numero (
			id_category serial not null,
			category_name text,
			CONSTRAINT kcategory_$numero PRIMARY KEY (id_category));";
			$resulttre = $lista->query($querytre);

			$queryresult = "CREATE TABLE result_$numero (
			id serial not null,
			card_name text,
			CONSTRAINT k_result_$numero PRIMARY KEY (id));";
			$resultcin = $lista->query($queryresult);
		}
		if($type == 'tree'){
			$querytre = "CREATE TABLE studies_category_$numero (
			id_category serial not null,
			category_name text,
			CONSTRAINT kcategory_$numero PRIMARY KEY (id_category));";
			$resulttre = $lista->query($querytre);

			$queryquattro = "CREATE TABLE studies_task_$numero (
			id_task serial not null, 
			task_name text,
			category text,
			sottocategory text,
			n_ok integer DEFAULT 0,
			n_wrong integer DEFAULT 0,
			CONSTRAINT ktask_$numero PRIMARY KEY (id_task));";
			$resultquattro = $lista->query($queryquattro);

			$querycinq = "CREATE TABLE studies_user_task_$numero (
			id serial not null, 
			id_task integer, 
			task_name text,
			category text,
			sottocategory text,
			correct integer,
			CONSTRAINT ktask_user_$numero PRIMARY KEY (id));";
			$resultcinq = $lista->query($querycinq);
		}


		return  $numero;
	}
   //memorizza i risultati di uno studio (card sorting aperto e chiuso)
public function aggiungi_occorrenza($card, $categoria, $studio){
$categoria = strtolower($categoria);
//controlla se vi è già la categoria, se c'è aggiungi 1 altrimenti aggiungi nuova riga
	$lista = new Db();
	$query = "SELECT * FROM studies_openclosed_id_$studio WHERE card_name = '$card' AND LOWER(category_name) = '$categoria'";

	$result = $lista->query($query);

		$numrows = mysqli_num_rows($result);

//aggiungi categoria se non presente
		$querycat = "SELECT * FROM studies_category_$studio WHERE LOWER(category_name) LIKE '$categoria'";
		$resultcin = $lista->query($querycat);
		$num = mysqli_num_rows($resultcin);
		$categoriaa = str_replace(' ', '_', $categoria);
		if ($num == 1){
			
			$queryres = "UPDATE result_$studio SET $categoriaa = $categoriaa + 1 WHERE card_name = '$card'";
			$resres = $lista->query($queryres);

		}else{
		//aggiungi categoria nella lista categorie
  			 $queryf = "INSERT INTO studies_category_$studio (category_name) VALUES ('$categoria')";
  			 $resultquattro = $lista->query($queryf);

  			 $queryres = "ALTER TABLE result_$studio ADD COLUMN $categoriaa integer DEFAULT 0;";
  			 $resres = $lista->query($queryres);

  			 $queryrest = "UPDATE result_$studio SET $categoriaa = $categoriaa + 1 WHERE card_name = '$card'";
			$resrest = $lista->query($queryrest);
	
		}




		if ($numrows == 1) {
			//categoria gia presente, aggiorna numero occorrenza
			 
   			$queryd = "UPDATE studies_openclosed_id_$studio SET n_occorrenza = n_occorrenza + 1 WHERE card_name = '$card' AND category_name = '$categoria'";
   			$resultdue = $lista->query($queryd);
		} else {
			//nuova categoria, aggiungi riga
			$queryt = "INSERT INTO studies_openclosed_id_$studio (card_name, category_name, n_occorrenza) VALUES ('$card', '$categoria', 1)";
  			 $resultre = $lista->query($queryt);
  			 
		}

		

return $numrows;
}

public function aggiungi_tabella_provvisoria($user){

$lista = new Db();
  $query = "CREATE TABLE user_category_$user (
			id_category serial not null,
			category_name text,
			CONSTRAINT kcategory_$user PRIMARY KEY (id_category));";
			$result = $lista->query($query);
return $result;
}

public function elimina_tab_provvisoria($user){
	$lista = new Db();
	$query = "DROP TABLE user_category_$user";
	$result = $lista->query($query);
return $result;
}

public function aggiungi_cat_prov($user, $element){

$lista = new Db();
   $query = "INSERT INTO user_category_$user (category_name) VALUES ('$element')";
   $result = $lista->query($query);

return $result;
}
public function modifica_categoria_utente($user, $modificata, $vecchia){
	$lista = new Db();
	$query = "UPDATE user_category_$user SET category_name = '$modificata' WHERE category_name = '$vecchia'";
	$result = $lista->query($query);
	return $result;
}
public function recupera_categorie_user($user) {
		$lista = array();

		$listaa = new Db();
		$query = "SELECT  * FROM user_category_$user";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$lista[] = new Categoria($risultati['category_name']);

		}
	
		return $lista;
	}


public function aggiungi_card($numstudio, $card){

$lista = new Db();
   $query = "INSERT INTO studies_openclosed_id_$numstudio (card_name) VALUES ('$card')";
   $result = $lista->query($query);

      $query = "INSERT INTO result_$numstudio (card_name) VALUES ('$card')";
   $result = $lista->query($query);

   $listaa = new Db();
   $queryd = "UPDATE studies SET n_card = n_card + 1 WHERE id_studies = $numstudio";
   $resultdue = $listaa->query($queryd);
return $result;
}
public function elimina_card($numstudio, $card){

$lista = new Db();
   $query = "DELETE FROM studies_openclosed_id_$numstudio WHERE card_name = '$card'";
   $result = $lista->query($query);

      $query = "DELETE FROM result_$numstudio WHERE card_name ='$card'";
   $result = $lista->query($query);

   $listaa = new Db();
   $queryd = "UPDATE studies SET n_card = n_card - 1 WHERE id_studies = $numstudio";
   $resultdue = $listaa->query($queryd);
return $result;
}

public function aggiungi_collegamento($numstudio, $sottocategoria, $categoria){

$lista = new Db();
   $query = "INSERT INTO studies_openclosed_id_$numstudio (card_name, category_name) VALUES ('$sottocategoria', '$categoria')";
   $result = $lista->query($query);

   $listaa = new Db();
   $queryd = "UPDATE studies SET n_card = n_card + 1 WHERE id_studies = $numstudio";
   $resultdue = $listaa->query($queryd);
return $result;
}
public function elimina_collegamento($numstudio, $sottocategoria, $categoria){

$lista = new Db();
   $query = "DELETE FROM studies_openclosed_id_$numstudio WHERE card_name = '$sottocategoria' AND category_name = '$categoria'";
   $result = $lista->query($query);

   $listaa = new Db();
   $queryd = "UPDATE studies SET n_card = n_card - 1 WHERE id_studies = $numstudio";
   $resultdue = $listaa->query($queryd);
return $result;
}
public function aggiorna_stato_attivo($numstudio){
	$lista = new Db();
	$query = "UPDATE studies SET state = 2, data_attivazione = now() WHERE id_studies = $numstudio";
	$result = $lista->query($query);
return $result;

}
public function aggiorna_stato_disattivo($numstudio){
	$lista = new Db();
	$query = "UPDATE studies SET state = 3, data_disattivazione = now() WHERE id_studies = $numstudio";
	$result = $lista->query($query);
return $result;

}



public function controlla_stato_studio($numstudio){
$listanumero = new Db();
		$querynumero = "SELECT state FROM studies WHERE id_studies = $numstudio";
		$resultnumero = $listanumero->query($querynumero);
			while ($risultati = mysqli_fetch_assoc($resultnumero)){

				$numero = $risultati['state'];
			}
	return $numero;
}

public function aggiungi_task($numstudio, $task, $id_percorso){

$lista = new Db();
$query = "SELECT card_name, category_name FROM studies_openclosed_id_$numstudio WHERE id_card = $id_percorso";
$result = $lista->query($query);
while ($com = mysqli_fetch_assoc($result)) {
				$percorso[] = [$com['card_name'], $com['category_name']];
			}
$categoria = $percorso[0][1];
$sottocategoria = $percorso[0][0];
$querydue = "INSERT INTO studies_task_$numstudio (task_name, category, sottocategory) VALUES ('$task', '$categoria', '$sottocategoria')";
$resultdue = $lista->query($querydue);

   $queryd = "UPDATE studies SET n_task = n_task + 1 WHERE id_studies = $numstudio";
   $resultdue = $lista->query($queryd);
return $result;
}
public function elimina_task($numstudio, $task){

$lista = new Db();

$querydue = "DELETE FROM studies_task_$numstudio WHERE id_task = $task";
$resultdue = $lista->query($querydue);

   $queryd = "UPDATE studies SET n_task = n_task - 1 WHERE id_studies = $numstudio";
   $result = $lista->query($queryd);
return $result;
}


public function aggiungi_categoria($numstudio, $categoria){

$lista = new Db();
   $query = "INSERT INTO studies_category_$numstudio (category_name) VALUES ('$categoria')";
   $result = $lista->query($query);
$categoria = str_replace(' ', '_', $categoria);
$queryres = "ALTER TABLE result_$numstudio ADD COLUMN $categoria integer DEFAULT 0;";
  			 $resres = $lista->query($queryres);

   $listaa = new Db();
   $queryd = "UPDATE studies SET n_categories = n_categories + 1 WHERE id_studies = $numstudio";
   $resultdue = $listaa->query($queryd);
return $result;
}

public function elimina_categoria($numstudio, $categoria){

$lista = new Db();
   $query = "DELETE FROM studies_category_$numstudio WHERE category_name = '$categoria'";
   $result = $lista->query($query);
$categoria = str_replace(' ', '_', $categoria);
$queryres = "ALTER TABLE result_$numstudio DROP COLUMN $categoria";
  			 $resres = $lista->query($queryres);

   $listaa = new Db();
   $queryd = "UPDATE studies SET n_categories = n_categories - 1 WHERE id_studies = $numstudio";
   $resultdue = $listaa->query($queryd);
return $result;
}

public function aggiungi_titolo($numstudio, $titolo){

$lista = new Db();
   $query = "UPDATE studies SET title = '$titolo' WHERE id_studies = $numstudio";
   $result = $lista->query($query);
return $result;
}
public function aggiungi_istruzioni($numstudio, $istruzioni){

$lista = new Db();
   $query = "UPDATE studies SET description = '$istruzioni' WHERE id_studies = $numstudio";
   $result = $lista->query($query);
return $result;
}

public function aggiungi_task_ok($numstudio, $numtask){

   $listaa = new Db();
   $queryd = "UPDATE studies_task_$numstudio SET n_ok = n_ok + 1 WHERE id_task = $numtask";
   $result = $listaa->query($queryd);
return $result;
}

public function aggiungi_task_user_one($numstudio, $idtaskn, $taskn, $categ, $sottocat){
	$lista = new Db();
	$query = "INSERT INTO studies_user_task_$numstudio (id_task, task_name, category, sottocategory, correct) VALUES ($idtaskn, '$taskn', '$categ', '$sottocat', 1);";
	$result = $lista->query($query);
return $result;
}

public function aggiungi_task_user_zero($numstudio, $idtaskn, $taskn, $sottocat){
	$lista = new Db();
	$querycategoria = "SELECT category_name FROM studies_openclosed_id_$numstudio WHERE card_name = '$sottocat'";
		$resultnumero = $lista->query($querycategoria);
			while ($risultati = mysqli_fetch_assoc($resultnumero)){

				$categoria = $risultati['category_name'];
			}
	$query = "INSERT INTO studies_user_task_$numstudio (id_task, task_name, category, sottocategory, correct) VALUES ($idtaskn, '$taskn', '$categoria', '$sottocat', 0);";
	$result = $lista->query($query);
return $result;
}

public function aggiungi_task_wrong($numstudio, $numtask){

$listaa = new Db();
   $queryd = "UPDATE studies_task_$numstudio SET n_wrong = n_wrong + 1 WHERE id_task = $numtask";
   $result = $listaa->query($queryd);
return $result;
}
	
public function aggiungi_n_partecipanti($numstudio){

$listaa = new Db();
   $queryd = "UPDATE studies SET n_partecipant = n_partecipant + 1 WHERE id_studies = $numstudio";
   $result = $listaa->query($queryd);
return $result;
}
	
public function recupera_studi($id_utente) {
		$cardsorting = array();

		$lista = new Db();
		$query = "SELECT  * FROM studies WHERE id_user = $id_utente ORDER BY state DESC";
		$result = $lista->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$cardsorting[] = new Studi($risultati['id_studies'], $risultati['type'], $risultati['n_card'], $risultati['n_categories'], $risultati['n_task'], $risultati['n_partecipant'], $risultati['link'], $risultati['title'], $risultati['description'], $risultati['data_creazione'], $risultati['state'], $risultati['data_attivazione'], $risultati['data_disattivazione']);

		}
	
		return $cardsorting;
	}


public function recupera_card($id_studio) {
		$lista = array();

		$listaa = new Db();
		$query = "SELECT DISTINCT(card_name) FROM studies_openclosed_id_$id_studio;";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$lista[] = new Card($risultati['card_name']);

		}
	
		return $lista;
	}

public function recupera_card_seq($id_studio) {
		$lista = array();

		$listaa = new Db();
		$query = "SELECT DISTINCT(card_name) FROM studies_openclosed_id_$id_studio;";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			array_push($lista, $risultati['card_name']);

		}
	
		return $lista;
	}


public function recupera_categorie($id_studio) {
		$lista = array();

		$listaa = new Db();
		$query = "SELECT  * FROM studies_category_$id_studio";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$lista[] = new Categoria($risultati['category_name']);

		}
	
		return $lista;
	}
public function recupera_schema($id_studio) {
		$lista = array();

		$listaa = new Db();
		$query = "SELECT  * FROM studies_openclosed_id_$id_studio ORDER BY category_name";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$lista[] = new Schema($risultati['id_card'], $risultati['card_name'], $risultati['category_name']);

		}
	
		return $lista;
	}

public function recupera_task($id_studio) {
		$lista = array();

		$listaa = new Db();
		$query = "SELECT  * FROM studies_task_$id_studio ORDER BY id_task";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$lista[] = new Task($risultati['id_task'], $risultati['task_name'], $risultati['category'], $risultati['sottocategory'], $risultati['n_ok'], $risultati['n_wrong']);

		}
	
		return $lista;
	}

public function recupera_risposte_task($id_studio) {
		$lista = array();

		$listaa = new Db();
		$query = "SELECT  * FROM studies_user_task_$id_studio ORDER BY id_task";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$lista[] = [$risultati['id_task'], $risultati['task_name'], $risultati['category'], $risultati['sottocategory'], $risultati['correct']];

		}
	
		return $lista;
	}

	public function recupera_info_studio($id_studio) {
	

		$listaa = new Db();
		$query = "SELECT  * FROM studies WHERE id_studies = $id_studio";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$cardsorting[] = [$risultati['id_studies'], $risultati['type'], $risultati['n_card'], $risultati['n_categories'], $risultati['n_task'], $risultati['n_partecipant'], $risultati['link'], $risultati['title'], $risultati['description'], $risultati['data_creazione'], $risultati['state'], $risultati['data_attivazione'], $risultati['data_disattivazione']];
		}
	
		return $cardsorting;
	}

public function recupera_risultati_studio($id_studio) {
		$lista = array();

		$listaa = new Db();
		$query = "SELECT * FROM studies_openclosed_id_$id_studio WHERE category_name <> ''  ORDER BY card_name, n_occorrenza DESC";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$lista[] = new Card_Result($risultati['id_card'], $risultati['card_name'], $risultati['category_name'], $risultati['n_occorrenza']);

		}
	
		return $lista;
	}
	public function recupera_tabella($id_studio) {
	

		$listaa = new Db();
		$query = "SELECT  * FROM result_$id_studio";
		$result = $listaa->query($query);

		while ($risultati = mysqli_fetch_assoc($result)) {

			$tabella[] = [$risultati];
		}
	
		return $tabella;
	}

public function statistica_categorie_distinte($numstudio){
$listanumero = new Db();
		$querynumero = "SELECT count(DISTINCT category_name) AS numero FROM studies_openclosed_id_$numstudio";
		$resultnumero = $listanumero->query($querynumero);
			while ($risultati = mysqli_fetch_assoc($resultnumero)){

				$numero = $risultati['numero'];
			}
	return $numero;
}

public function statistica_card_categorie($numstudio){
	$lista = array();
	$lista_card = array();
$listanumero = new Db();
		$querynumero = "SELECT category_name, count(card_name) AS numero FROM studies_openclosed_id_$numstudio WHERE category_name <> '' GROUP BY category_name ORDER BY numero DESC";
		$resultnumero = $listanumero->query($querynumero);
			while ($risultati = mysqli_fetch_assoc($resultnumero)){
				$categoria = $risultati['category_name'];
				$query = "SELECT card_name, n_occorrenza FROM studies_openclosed_id_$numstudio WHERE category_name = '$categoria' ORDER BY n_occorrenza DESC";
				$result = $listanumero->query($query);
				while ($risultatic = mysqli_fetch_assoc($result)){
					$lista_card[] = $risultatic['card_name'];
					$lista_card[] = $risultatic['n_occorrenza']; 
				}
				$lista[] = [$risultati['category_name'], $risultati['numero'], $lista_card];
				$lista_card = array();
			}
	return $lista;
}
	
}
?>