<?php

class Card {
	// attributi
	public $nome;


	public function __construct($unNome) {
		$this->nome = $unNome;
		}

}

class Schema {
	// attributi
	public $id;
	public $sottoc;
	public $nomec;


	public function __construct($unId, $unNome, $unNomec) {
		$this->id = $unId;
		$this->sottoc = $unNome;
		$this->nomec = $unNomec;
		}

}

class Categoria {
	// attributi
	public $cat;


	public function __construct($unNome) {
		$this->cat = $unNome;
		}

}

class Task {
	// attributi
	public $idtaskn;
	public $taskn;
	public $categ;
	public $sottocat;
	public $ok;
	public $wrong;

	public function __construct($unId, $unNome, $unNom, $unNo, $unOk, $unWrong) {
		$this->idtaskn = $unId;
		$this->taskn = $unNome;
		$this->categ = $unNom;
		$this->sottocat = $unNo;
		$this->ok = $unOk;
		$this->wrong = $unWrong;
		}

}

class Card_Result {
	// attributi
	public $id;
	public $nome;
	public $catnome;
	public $numero;


	public function __construct($unId, $unNome, $unCat, $unNum) {
		$this->id = $unId;
		$this->nome = $unNome;
		$this->catnome = $unCat;
		$this->numero = $unNum;
		}

}

class Studi {
	// attributi
	public $id_study;
	public $type;
	public $ncards;
	public $ncategory;
	public $ntask;
	public $npartecipant;
	public $link;
	public $title;
	public $istruzioni;
	public $data;
	public $stato;
	public $inizio;
	public $fine;

	public function __construct($unId, $unNome, $unaDurata, $unSi, $unNo, $unCat, $unLink, $unTitolo, $unIstru, $unaData , $unoStato, $unInizio, $unaFine) {
		$this->id_study = $unId;
		$this->type = $unNome;		
		$this->ncards = $unaDurata;
		$this->ncategory = $unSi;
		$this->ntask = $unNo;
		$this->npartecipant = $unCat;
		$this->link = $unLink;
		$this->title = $unTitolo;
		$this->istruzioni = $unIstru;
		$this->data = $unaData;
		$this->stato = $unoStato;
		$this->inizio = $unInizio;
		$this->fine = $unaFine;
}




}


?>