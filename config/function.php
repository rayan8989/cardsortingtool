<?php
function change_key( $array, $old_key, $new_key ) {

    if( ! array_key_exists( $old_key, $array ) )
        return $array;

    $keys = array_keys( $array );
    $keys[ array_search( $old_key, $keys ) ] = $new_key;

    return array_combine( $keys, $array );
}
function replaceKeys($input) {

    $return = array();
    foreach ($input as $key => $value) {
        if (strpos($key, '_tot') === 0)
            $key = substr($key, 1);

        if (is_array($value))
            $value = replaceKeys($value); 

        $return[$key] = $value;
    }
    return $return;
}
function Tipo_s($tipo_studio){
  if($tipo_studio == 'open'){
    $tipo_studio = "Card Sort Aperto";
  }elseif($tipo_studio == 'closed'){
    $tipo_studio = "Card Sort Chiuso";
  }elseif($tipo_studio == 'tree'){
    $tipo_studio = "Tree Testing";
  }
return $tipo_studio;
}
function Tipo_s_bre($tipo_studio){
  if($tipo_studio == 'open'){
    $tipo_studio = "[CS-A]";
  }elseif($tipo_studio == 'closed'){
    $tipo_studio = "[CS-C]";
  }elseif($tipo_studio == 'tree'){
    $tipo_studio = "[TT]";
  }
return $tipo_studio;
}
function Check_zero($numero, $tipo){
  if(strcmp($numero, "0") == 0){
    
    $numero = "";
  }else {
    $numero = '<span class="time">'.$numero.' '.$tipo.'</span>';
  }
return $numero;
}

function escapeline($line) {
  $line = str_replace(" ", '_', $line);
  $line = str_replace("’", '0', $line);
  $line = str_replace("'", '0', $line);
  $line = str_replace("/", '', $line);
  $line = str_replace(",", '', $line);
  $line = str_replace('”', '1', $line);
  $line = str_replace('“', '1', $line);
  $line = str_replace('"', '1', $line);
  $line = str_replace(';', '', $line);
  $line = str_replace('.', '', $line);
  $line = str_replace('?', '', $line);
  $line = str_replace('!', '', $line);
  $line = str_replace('(', '', $line);
  $line = str_replace(')', '', $line);
  $line = str_replace('[', '', $line);
  $line = str_replace(']', '', $line);
  $line = str_replace('}', '', $line);
  $line = str_replace('{', '', $line);
  return $line;
}
function escapelineInverse($line) {
  $line = str_replace("_", ' ', $line);
  $line = str_replace("’", '0', $line);
  $line = str_replace("'", '0', $line);
  $line = str_replace("/", '', $line);
  $line = str_replace(",", '', $line);
  $line = str_replace('”', '1', $line);
  $line = str_replace('“', '1', $line);
  $line = str_replace('"', '1', $line);
  $line = str_replace(';', '', $line);
  $line = str_replace('.', '', $line);
  $line = str_replace('?', '', $line);
  $line = str_replace('!', '', $line);
  $line = str_replace('(', '', $line);
  $line = str_replace(')', '', $line);
  $line = str_replace('[', '', $line);
  $line = str_replace(']', '', $line);
  $line = str_replace('}', '', $line);
  $line = str_replace('{', '', $line);
  return $line;
}

function Corr($x, $y){

$length= count($x);
$mean1=array_sum($x) / $length;
$mean2=array_sum($y) / $length;

$a=0;
$b=0;
$axb=0;
$a2=0;
$b2=0;

for($i=0;$i<$length;$i++)
{
$a=$x[$i]-$mean1;
$b=$y[$i]-$mean2;
$axb=$axb+($a*$b);
$a2=$a2+ pow($a,2);
$b2=$b2+ pow($b,2);
}

$corr= $axb / sqrt($a2*$b2);

return $corr;
}


function ritornacategoria($x){

switch ($x) {
  case 1:
    $categoriatrovata = 'informatica';
    break;
    case 2:
   $categoriatrovata = 'fisica';
    break;
    case 3:
   $categoriatrovata = 'chimica';
    break;
    case 5:
    $categoriatrovata = 'giurisprudenza';
    break;
    case 6:
    $categoriatrovata = 'lingue';
    break;
    case 7:
    $categoriatrovata = 'elettronica';
    break;
    case 9:
   $categoriatrovata = 'cucina';
    break;
    case 10:
   $categoriatrovata = 'arte';
    break;
    case 11:
   $categoriatrovata = 'economia';
    break;
    case 12:
   $categoriatrovata = 'matematica';
    break;
  
  default:
    # code...
    break;
}


return $categoriatrovata;
}
//Conversione fra due valute
// uso: echo currency("USD","EUR",100);
function currency($from_Currency,$to_Currency,$amount) {
	$amount = urlencode($amount);
	$from_Currency = urlencode($from_Currency);
	$to_Currency = urlencode($to_Currency);
	$url = "http://www.google.com/ig/calculator?hl=en&q=$amount$from_Currency=?$to_Currency";
	$ch = curl_init();
	$timeout = 0;
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$rawdata = curl_exec($ch);
	curl_close($ch);
	$data = explode('"', $rawdata);
	$data = explode(' ', $data['3']);
	$var = $data['0'];
	return round($var,2);
}
//Ridimensionamento proporzionale immagine
//echo scale_image('images/immagine.jpg', '500');
function scale_image($foto, $maxdim){
 	$thumb_width = $maxdim;
 	list($fullsize_width, $fullsize_height) = getimagesize($foto);
 	$thumb_height = @floor($fullsize_height/($fullsize_width/$thumb_width));
	 if($fullsize_width > $thumb_width){ $width = $thumb_width; $height = $thumb_height; } else { $width = $fullsize_width; $height = $fullsize_height; }
 
 	return '<img src="' . $foto . '" width="' . $width . '" height="' . $height . '" />';
}

	//Calcolo dell’iva
   // echo iva('1000'); //restituirà il prezzo compreso d'iva (1210)
function iva($prezzo) { 
    $iva = 21; //definire costante dell'iva (21%)
    $totale = $prezzo + (($prezzo / 100) * $iva);
    $totale = round($totale, 2);
 
    return $totale; 
}

//Verifica presenza caratteri speciali in una stringa
// check_char('stringa_alfanumerica21457'); //restituirà il rispettivo messaggio
function check_char($string){
    if (ereg('[^A-Za-z0-9]', $string)) 
      echo "La stringa contiene caratteri speciali"; 
        else
        echo "La stringa non contiene caratteri speciali";     
}

//Calcolo età
//Utilizzo: echo birthday('1986-07-22');
function birthday($birthday){ 
    $age = strtotime($birthday); 
    if($age === FALSE){ 
        return FALSE; 
    } 
    list($y1,$m1,$d1) = explode("-",date("Y-m-d",$age)); 
    $now = strtotime("now"); 
    list($y2,$m2,$d2) = explode("-",date("Y-m-d",$now)); 
    $age = $y2 - $y1; 
    if($m2-$m1 < 0 || $d2-$d1 > 0) 
        $age--; 
    return $age; 
}

//Validazione Email
//Utilizzo: check_email('nome@dominio.it');

function check_email($email){
  if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)){
    $controllamail=true;
     return $controllamail;
       } else {
          $controllamail=false;
          return $controllamail; }
  }


 //Generazione random di password
 //Generatore automatico di stringhe che può essere sfruttata come ottima soluzione per le password automatiche, di default a 7 caratteri.

function createRandomPassword() { 
    $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
    srand((double)microtime()*1000000); 
    $i = 0; 
    $pass = '' ; 
 
    while ($i <= 7) { 
        $num = rand() % 33; 
        $tmp = substr($chars, $num, 1); 
        $pass = $pass . $tmp; 
        $i++; 
    } 
 
    return $pass; 
 
} 
 
// Usage 
//$password = createRandomPassword(); 
//echo "Your random password is: $password";
//Utilizzo:
//echo createRandomPassword();





//Da timestamp a formato data
//Utilizzo:

//echo timestamp_to_date('timestamp_number');


function timestamp_to_date($timestamp){
   return date("r", $timestamp);
}

//Da formato data a timestamp
//Utilizzo:

//echo date_to_timestamp(22-09-2008', '%d-%m-%Y');
function date_to_timestamp($data, $formato){
  $a = strptime($data, $formato);
  $timestamp = mktime(0, 0, 0, $a['tm_month'], $a['tm_day'], $a['tm_year']);
}
//mostra i messaggi di servizio tramite Alert messagge con Javascript
  function showAlert($msg){
echo"
<script type=\"text/javascript\">
alert( '$msg');
</script>
";

  }
?>