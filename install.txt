Per installare il sistema:
1. Installare PHP e MySQL sul proprio server o pc.
2. Creare il database "card sorting" e le relative tabelle eseguendo il file "install.sql" da linea di comando o caricandolo tramite MySqlAdmin.
3. Cambiare i parametri in config/config.php inserendo la propria password e eventualmente modificando l'host.

Ora dopo aver fatto partire il server PHP e il database MySql, aprire il browser e digitare "localhost".