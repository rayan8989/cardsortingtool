CREATE DATABASE cardsorting;


-- Table: utenti

-- DROP TABLE utenti;

CREATE TABLE utenti
(
  id_user serial NOT NULL,
  username text,
  name text,
  password text,
  PRIMARY KEY id_user (id_user )
);




-- Table: studies

-- DROP TABLE studies;

CREATE TABLE studies
(
  id_studies serial NOT NULL,
  type text,
  n_card integer DEFAULT 0,
  n_categories integer DEFAULT 0,
  n_task integer DEFAULT 0,
  n_partecipant integer DEFAULT 0,
  link text,
  id_user integer,
  title text,
  description text,
  data_creazione text,
  state integer,
  data_attivazione text,
  data_disattivazione text,
  PRIMARY KEY id_studies(id_studies ),
  FOREIGN KEY (id_user) REFERENCES utenti(id_user)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
