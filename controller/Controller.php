<?php

include_once ROOT . DS . "model" . DS . "Module.php";
include_once ROOT . DS . "config" . DS . "function.php";

//IL CONTROLLER CONTROLLO CONTROLLA IL LOGIN, SE VA A BUON FINE PASSA AL CONTROLLER MENU CHE GESTISCE I CONTROLLI PER TUTTE LE SEZIONI
class Controllo {
	// attributo per un oggetto Modello
	private $modello;

	public function __construct() {
		$this->modello = new Modello();
	}

	public function invoke() {

		$comando = isset($_REQUEST["comando"]) ?
		$_REQUEST["comando"] : "paginainiziale";
		// leggo il comando, per default "paginainiziale"

		if ($comando == "paginainiziale") {
			session_start();

			//se la variabile session è settata, identificichiamo l'utente
			if (isset($_SESSION['username'])) {
				$username = $_SESSION['username'];
			
				
				include ROOT . DS . "home.php";
				
			} else {

				include ROOT . DS . "template" . DS . "login.php";
			}
			//header("location: template/login.php");

			// passo il controllo alla vista "paginainziale.php"

			// include("view/tpl_pagina_iniziale.php");
		} elseif ($comando == "logout") {
			session_unset();
			session_destroy();
			include ROOT . DS . "template" . DS . "login.php";

		}
		//CONTROLLA LOGIN UTENTE
		elseif ($comando == "login") {

			//controlla se i campi sono inseriti correttamente
			$email = $_POST['username'];
			$password = md5($_REQUEST['password']);

				//controlla se user presente nel database
				$login = $this->modello->recupera_login($email, $password);
				if ($login != null) {
				


						session_start();

						
						$_SESSION['id'] = $login[0][0];
						
						$_SESSION['username'] = $login[0][1];
						$username = $_SESSION['username'];
						$id_utente = $_SESSION['id'];

						$comando == "home";
						$msg = 'autenticazione eseguita con successo';
						//showAlert($msg);
					
						//include("view/tpl_menu.php");
						header("location: home.php");
						include ROOT .  DS . "home.php";
						//include ROOT . DS . "template" . DS . "index.php";

				
				} else {
					//email non presente nel database
					$msg = "username inesistente o password incoretta";
					showAlert($msg);
					//include("view/tpl_pagina_iniziale.php");
					include ROOT . DS . "template" . DS . "login.php";
				}
		

		}
		elseif ($comando == "registrati") {

		
		
			include ROOT . DS . "registrati.php";

		}
		elseif  ($comando == "registrazione"){

			
			$username = $_POST["username"];
			$password = md5($_POST["password"]);
				//controlla che username sia libero
				$checkuser = $this->modello->recupera_user($username);
				if ($checkuser == 0) {

					//echo 'libero';
					$registrazione = $this->modello->registra_utente($username, $password);
						//se libero allora registra altrimenti ritorna a registrati.php
					//registra utente e setta +4 alla categoria selezionata
					if ($registrazione != null){
						session_start();
						$_SESSION['username'] = $username;


						$comando == "login";
						$msg = 'registrazione eseguita con successo';
						//showAlert($msg);
					
						include ROOT . DS . "template" . DS . "login.php";
					}

				}
			
				else{
					$msg = "usernname già in uso";
					showAlert($msg);
					//include("view/tpl_pagina_iniziale.php");
					include ROOT . DS . "template" . DS . "login.php";
				}
			}

		}
}
//FINE CONTROLLER PER LOGIN

//INIZIO CONTROLLER utente admin
class Controlloutente extends Controllo {

	private $modello;

	public function __construct() {
		$this->modello = new Modello();
	}

	public function invoke() {
		// leggo il comando, per default "paginainiziale"
		$comando = isset($_REQUEST["comando"]) ?
		$_REQUEST["comando"] : "home";

		session_start();
		$username = $_SESSION['username'];
		$id_utente = $_SESSION['id'];
		//echo json_encode($_SESSION);
		

		if ($comando == "home") {
	$lista_card_sort = $this->modello->recupera_studi($id_utente);


			include ROOT . DS . "template" . DS . "home.php";

		}  elseif ($comando == "logout") {
			session_unset();
			session_destroy();
			include ROOT . DS . "template" . DS . "login.php";
		}
		 elseif ($comando == "new") {


		 	//crea studio e tabella studio
		 	if (isset($_REQUEST["type"])) {
				$type = $_REQUEST["type"];
				//echo $type;

				//TRE CASE 
				if ($type == "open"){
					$aggiungi_studio = $this->modello->aggiungi_studio($type, $id_utente);
					$id_studio = $aggiungi_studio;
					$_SESSION['studio']	= $id_studio;
					$messagge = "Studio ".$id_studio." registrato con successo!";
					//$lista_card = $this->modello->recupera_card($id_studio);
					showAlert($messagge);
					header("location: home.php?comando=edit&tipostudio=open&numerostudio=".$id_studio);
					//include ROOT . DS . "template" . DS . "opencardsortsetting.php";
				}
					elseif ($type == "closed"){
						$aggiungi_studio = $this->modello->aggiungi_studio($type, $id_utente);
						$id_studio = $aggiungi_studio;
						$_SESSION['studio']	= $id_studio;
						$messagge = "Studio ".$id_studio." registrato con successo!";
						//$lista_card = $this->modello->recupera_card($id_studio);
						showAlert($messagge);
				header("location: home.php?comando=edit&tipostudio=closed&numerostudio=".$id_studio);

					}
						elseif ($type == "tree"){
								$aggiungi_studio = $this->modello->aggiungi_studio($type, $id_utente);
						$id_studio = $aggiungi_studio;
						$_SESSION['studio']	= $id_studio;
						$messagge = "Studio ".$id_studio." registrato con successo!";
						//$lista_card = $this->modello->recupera_card($id_studio);
						showAlert($messagge);
				header("location: home.php?comando=edit&tipostudio=tree&numerostudio=".$id_studio);

						}


				//aggiungi studio e crea tabella studio
				
				//exit();
				}
			
	

				
			

			
		}
		 elseif ($comando == "edit") {

		 	$lista_card_sort = $this->modello->recupera_studi($id_utente);

			if (isset($_REQUEST["numerostudio"])){
				$numstudio = $_REQUEST["numerostudio"];
		 			//crea studio e tabella studio



		 		if (isset($_REQUEST["tipostudio"])) {
					$type = $_REQUEST["tipostudio"];
				//echo $type;

				//TRE CASE 
					if ($type == "open"){
							
						if (isset($_REQUEST["comune"])){
							$card= $_REQUEST["comune"];
						//inserisci card nel db
							$inserisci_card = $this->modello->aggiungi_card($numstudio, $card);
						//echo $_REQUEST["comune"];
							$messagge = "Card inserita con successo!";
							//$lista_card = $this->modello->recupera_card($id_studio);
							//showAlert($messagge);
							$lista_card = $this->modello->recupera_card($numstudio);
							include ROOT . DS . "template" . DS . "tabellacard.php";
							exit();
						}
						
						if ((isset($_REQUEST["titolo"])) || (isset($_REQUEST["istruzioni"]))){
							if (isset($_REQUEST["titolo"])){
							$titolo = $_REQUEST["titolo"];
							$inserisci_titolo = $this->modello->aggiungi_titolo($numstudio, $titolo);
							$messagge = "Titolo inserito con successo!";
							}
							if (isset($_REQUEST["istruzioni"])){
							$istruzioni = $_REQUEST["istruzioni"];
							$inserisci_istruzioni = $this->modello->aggiungi_istruzioni($numstudio, $istruzioni);
							$messagge = "Descrizione inserita con successo!";
							//$lista_card = $this->modello->recupera_card($id_studio);
							//showAlert($messagge);
							}
							exit();

						}
						

						if (isset($_REQUEST["active"])){
							$stato = $_REQUEST["active"];
							if($stato == "yes"){
							$aggiorna_stato = $this->modello->aggiorna_stato_attivo($numstudio);
						}elseif($stato == "no"){
							$aggiorna_stato = $this->modello->aggiorna_stato_disattivo($numstudio);
						}	
							header("location: home.php");
						


						}
						if (isset($_REQUEST["cardelimina"])){
							$card_elimina = $_REQUEST["cardelimina"];
							$elimina_card = $this->modello->elimina_card($numstudio, $card_elimina);
							
							$lista_card = $this->modello->recupera_card($numstudio);
							include ROOT . DS . "template" . DS . "tabellacard.php";
							exit();


						}

							$info_studio = $this->modello->recupera_info_studio($numstudio);
							$lista_card = $this->modello->recupera_card($numstudio);
							$lista_risultati = $this->modello->recupera_risultati_studio($numstudio);
							include ROOT . DS . "template" . DS . "opencardsortsetting.php";
					}

					elseif ($type == "closed"){

						if (isset($_REQUEST["cardone"])){
							$card= $_REQUEST["cardone"];

							//inserisci card nel db
							$inserisci_card = $this->modello->aggiungi_card($numstudio, $card);
							//echo $_REQUEST["comune"];
							$messagge = "Card inserita con successo!";
								//$lista_card = $this->modello->recupera_card($id_studio);
							$lista_card = $this->modello->recupera_card($numstudio);
								include ROOT . DS . "template" . DS . "tabellacard.php";
								//showAlert($messagge);
							exit();
						}

						if (isset($_REQUEST["categoria"])){
							$categoria= $_REQUEST["categoria"];
							//inserisci card nel db
							$inserisci_categoria = $this->modello->aggiungi_categoria($numstudio, $categoria);
							//echo $_REQUEST["comune"];
							$messagge = "Categoria inserita con successo!";
								//$lista_card = $this->modello->recupera_card($id_studio);
							$lista_categorie = $this->modello->recupera_categorie($numstudio);
							
							include ROOT . DS . "template" . DS . "tabellacategorie.php";
								//showAlert($messagge);
							exit();
						}
						if (isset($_REQUEST["cardelimina"])){
							$card_elimina = $_REQUEST["cardelimina"];
							$elimina_card = $this->modello->elimina_card($numstudio, $card_elimina);
							
							$lista_card = $this->modello->recupera_card($numstudio);
							include ROOT . DS . "template" . DS . "tabellacard.php";
							exit();


						}
						if (isset($_REQUEST["categoriaelimina"])){
							$categoria_elimina = $_REQUEST["categoriaelimina"];
							$elimina_categoria = $this->modello->elimina_categoria($numstudio, $categoria_elimina);
							
							$lista_categorie = $this->modello->recupera_categorie($numstudio);
							
							include ROOT . DS . "template" . DS . "tabellacategorie.php";
							exit();


						}
						
						if ((isset($_REQUEST["titolo"])) || (isset($_REQUEST["istruzioni"]))){
							if (isset($_REQUEST["titolo"])){
							$titolo = $_REQUEST["titolo"];
							$inserisci_titolo = $this->modello->aggiungi_titolo($numstudio, $titolo);
							$messagge = "Titolo inserito con successo!";
							}
							if (isset($_REQUEST["istruzioni"])){
							$istruzioni = $_REQUEST["istruzioni"];
							$inserisci_istruzioni = $this->modello->aggiungi_istruzioni($numstudio, $istruzioni);
							$messagge = "Descrizione inserita con successo!";
							//$lista_card = $this->modello->recupera_card($id_studio);
							//showAlert($messagge);
							}
							exit();

						}

						if (isset($_REQUEST["active"])){
							$stato = $_REQUEST["active"];
							if($stato == "yes"){
							$aggiorna_stato = $this->modello->aggiorna_stato_attivo($numstudio);
						}elseif($stato == "no"){
							$aggiorna_stato = $this->modello->aggiorna_stato_disattivo($numstudio);
						}	
							header("location: home.php");
						


						}

					$info_studio = $this->modello->recupera_info_studio($numstudio);
					$lista_categorie = $this->modello->recupera_categorie($numstudio);
					$lista_card = $this->modello->recupera_card($numstudio);
					$lista_risultati = $this->modello->recupera_risultati_studio($numstudio);
					include ROOT . DS . "template" . DS . "closedcardsortsetting.php";

					}

					elseif ($type == "tree"){

						if ((isset($_REQUEST["titolo"])) || (isset($_REQUEST["istruzioni"]))){
							if (isset($_REQUEST["titolo"])){
							$titolo = $_REQUEST["titolo"];
							$inserisci_titolo = $this->modello->aggiungi_titolo($numstudio, $titolo);
							$messagge = "Titolo inserito con successo!";
							}
							if (isset($_REQUEST["istruzioni"])){
							$istruzioni = $_REQUEST["istruzioni"];
							$inserisci_istruzioni = $this->modello->aggiungi_istruzioni($numstudio, $istruzioni);
							$messagge = "Descrizione inserita con successo!";
							//$lista_card = $this->modello->recupera_card($id_studio);
							//showAlert($messagge);
							}
							exit();

						}


						if (isset($_REQUEST["categoria"])){
							$categoria= $_REQUEST["categoria"];
							//inserisci card nel db
							$inserisci_categoria = $this->modello->aggiungi_categoria($numstudio, $categoria );
							//echo $_REQUEST["comune"];
							$messagge = "Categoria inserita con successo!";
								//$lista_card = $this->modello->recupera_card($id_studio);
							$lista_categorie = $this->modello->recupera_categorie($numstudio);
							//showAlert($messagge);
							include ROOT . DS . "template" . DS . "categorieschema.php";
								
							exit();
						}

						if (isset($_REQUEST["sottocat"])){
							$socategoria= $_REQUEST["sottocat"];
							$ategoria= $_REQUEST["cate"];
							$inserisci_collegamento = $this->modello->aggiungi_collegamento($numstudio, $socategoria, $ategoria);	
							$messagge = "Collegamento inserito con successo.";
								//$lista_card = $this->modello->recupera_card($id_studio);
							$lista_schema = $this->modello->recupera_schema($numstudio);
							//showAlert($messagge);
							include ROOT . DS . "template" . DS . "schema.php";
							exit();
						}
						if ((isset($_REQUEST["sottocatelimina"])) && (isset($_REQUEST["categoriaelimina"]))){
							$socategoria= $_REQUEST["sottocatelimina"];
							$ategoria= $_REQUEST["categoriaelimina"];
							$elimina_collegamento = $this->modello->elimina_collegamento($numstudio, $socategoria, $ategoria);	
							$messagge = "Collegamento eliminato con successo.";
								//$lista_card = $this->modello->recupera_card($id_studio);
							$lista_schema = $this->modello->recupera_schema($numstudio);
							//showAlert($messagge);
							include ROOT . DS . "template" . DS . "schema.php";
							exit();
						}

						if (isset($_REQUEST["task"])){
							$task= $_REQUEST["task"];
							$id_percorso= $_REQUEST["percorso"];

							$inserisci_task = $this->modello->aggiungi_task($numstudio, $task, $id_percorso);	
							$messagge = "Task inserito con successo.";
							$lista_task = $this->modello->recupera_task($numstudio);
								//$lista_card = $this->modello->recupera_card($id_studio);
							include ROOT . DS . "template" . DS . "tabletask.php";
							exit();
						}
						if (isset($_REQUEST["taskelimina"])){
							$taskelimina= $_REQUEST["taskelimina"];
							
							$elimina_task = $this->modello->elimina_task($numstudio, $taskelimina);	
							$messagge = "Task eliminato con successo.";
							$lista_task = $this->modello->recupera_task($numstudio);
								//$lista_card = $this->modello->recupera_card($id_studio);
							include ROOT . DS . "template" . DS . "tabletask.php";
							exit();
						}

						if (isset($_REQUEST["active"])){
							$stato = $_REQUEST["active"];
							if($stato == "yes"){
							$aggiorna_stato = $this->modello->aggiorna_stato_attivo($numstudio);
						}elseif($stato == "no"){
							$aggiorna_stato = $this->modello->aggiorna_stato_disattivo($numstudio);
						}	
							header("location: home.php");
						


						}

						$info_studio = $this->modello->recupera_info_studio($numstudio);
						$lista_categorie = $this->modello->recupera_categorie($numstudio);
						$lista_schema = $this->modello->recupera_schema($numstudio);
						$lista_task = $this->modello->recupera_task($numstudio);
							
						include ROOT . DS . "template" . DS . "treesortsetting.php";
					}


				
				}
				
			}
			
			
		}elseif($comando == "upload"){
			if (isset($_REQUEST["numerostudio"])){
				$numstudio = $_REQUEST["numerostudio"];
				if (isset($_REQUEST["tipostudio"])) {
					$tipo = $_REQUEST["tipostudio"];
					if (isset($_REQUEST["txt"])) {
						$tipo_upload = $_REQUEST["txt"];

						  $uploaddir = '';
							$uploadfile = $uploaddir . basename($_FILES['fileInput']['name']);

							//echo '<pre>';
							if (move_uploaded_file($_FILES['fileInput']['tmp_name'], $uploadfile)) {
							    $result = "File is valid, and was successfully uploaded.\n";
							} else {
							    $result = "Possible file upload attack!\n";
							}

							//echo 'Here is some more debugging info:';
							//print_r($_FILES);
							//print "</pre>";
							$fp = fopen($uploadfile, 'rb');
										$i=0;	
							while ( ($line = fgets($fp)) !== false) {
								$line = mb_convert_encoding($line, "UTF-8", "Windows-1252");
								$line = str_replace("\r\n", '', $line);
								//echo $line;
								if($tipo_upload == "card"){
										$inserisci_card = $this->modello->aggiungi_card($numstudio, $line);
									}elseif ($tipo_upload == "category") {
										$inserisci_categoria = $this->modello->aggiungi_categoria($numstudio, $line);
									}
							
								
							}
						header("location: home.php?comando=edit&tipostudio=".$tipo."&numerostudio=".$numstudio);	
					}
				}
			}
		}elseif($comando == "result"){
			if (isset($_REQUEST["numerostudio"])){
				$numstudio = $_REQUEST["numerostudio"];
		 			//crea studio e tabella studio



		 		if (isset($_REQUEST["tipostudio"])) {
					$type = $_REQUEST["tipostudio"];
					$info_studio = $this->modello->recupera_info_studio($numstudio);
						if ($type == "open"){
							$categorie_totali = $this->modello->statistica_categorie_distinte($numstudio);
							$lista_categorie = $this->modello->recupera_categorie($numstudio);
							$lista_risultati = $this->modello->recupera_risultati_studio($numstudio);
							$lista_card_categorie = $this->modello->statistica_card_categorie($numstudio);
							$tabella_totale = $this->modello->recupera_tabella($numstudio);
							include ROOT . DS . "template" . DS . "resultopen.php";
						}
							elseif ($type == "closed"){
								$categorie_totali = $this->modello->statistica_categorie_distinte($numstudio);
							$lista_categorie = $this->modello->recupera_categorie($numstudio);
							$lista_risultati = $this->modello->recupera_risultati_studio($numstudio);
							$lista_card_categorie = $this->modello->statistica_card_categorie($numstudio);
							$tabella_totale = $this->modello->recupera_tabella($numstudio);
								include ROOT . DS . "template" . DS . "resultclosed.php";

							}
								elseif ($type == "tree"){
										$info_studio = $this->modello->recupera_info_studio($numstudio);
						$lista_categorie = $this->modello->recupera_categorie($numstudio);
						$lista_schema = $this->modello->recupera_schema($numstudio);
						$lista_task = $this->modello->recupera_task($numstudio);
						$lista_risposte = $this->modello->recupera_risposte_task($numstudio);
									include ROOT . DS . "template" . DS . "resulttree.php";

								}



					

				}else{echo'errore';}
			}else{echo'non sei autorizzato';}

		}elseif ($comando == "help") {

			include ROOT . DS . "template" . DS . "help.php";
		}
		 

	
	}
}
//FINE CONTROLLER PER utente admin

//INIZIO CONTROLLER utente 
class Controllocard {
	// attributo per un oggetto Modello
	private $modello;

	public function __construct() {
		$this->modello = new Modello();
	}

	public function invoke() {

		$comando = isset($_REQUEST["comando"]) ?
		$_REQUEST["comando"] : "do";


		$numstudio = $_REQUEST["studio"];	
		//echo $numstudio;
		$stato_studio = $this->modello->controlla_stato_studio($numstudio);
		if($stato_studio == 2) {


		session_start();
		if (empty($_SESSION["user"])){
		$user = rand();	
		$_SESSION["user"] = $user;
		//crea tabella utente
		$crea_tabella_utente = $this->modello->aggiungi_tabella_provvisoria($user);

		} else {
		$user = $_SESSION["user"];	
		}
		

		if ($comando == "do") {
			if (isset($_REQUEST["studio"])){
				$numstudio = $_REQUEST["studio"];
				$info_studio = $this->modello->recupera_info_studio($numstudio);
				//echo $numstudio;
				$type= $info_studio[0][1];
			
				if($type == "open"){

					
					if(isset($_REQUEST["addcategoria"])){
						$nuovacategoria = $_REQUEST["addcategoria"];
						//----->elimino spazi, apostrofi e altro
						$nuovacategoria = escapeLine($nuovacategoria);
						$lista_ordinamento = json_decode($_REQUEST["ordinamento"], true);
						//se l'utente ha fatto un ordinamento, aggiorna la session
						if((isset($lista_ordinamento)) && ($lista_ordinamento != null)){
							//print_r($lista_ordinamento);
							$card_selezionate_utente = array();
							//inserisco la categoria alla sessione
							array_push($_SESSION["categorie"], $nuovacategoria);
							//
							foreach($lista_ordinamento as $chiave =>$val){ 
						    //$_SESSION["ordinamento_utente"]["$chiave"] = array();
						    foreach($val as $valore){ 
				    	//$aggiungi_occorrenza = $this->modello->aggiungi_occorrenza($valore, $chiave, $numstudio);
						    	array_push($card_selezionate_utente, str_replace('_', ' ', $valore));
				      
    							} 
    							

    							$lista_ordinamento["$nuovacategoria"] = array();
    							//echo"</br>";
    							//print_r($lista_ordinamento);
    							//aggiorno le card rimanenti
    							$_SESSION["card"] = array_diff($_SESSION["card"], $card_selezionate_utente);
    							//inizializzo variabile sessione ordinamento
    							//$_SESSION["ordinamento_utente"] = 1;

	    				//$_SESSION["ordinamento_utente"] = [$chiaves];
	    				
							} 
							include ROOT . DS . "template" . DS . "usercategory.php";
							exit();
						}//altrimenti aggiungi solo la categoria alla sessione
						
						array_push($_SESSION["categorie"], $nuovacategoria);
						
						//print_r($_SESSION["categorie"]);

						include ROOT . DS . "template" . DS . "usercategory.php";
						exit();
					}

					if((isset($_REQUEST["modificata"])) && (isset($_REQUEST["modificata"]))){
						$modificata = $_REQUEST["modificata"];
						$modificata = escapeLine($modificata);
						$vecchia = $_REQUEST["vecchia"];
						$lista_ordinamento = json_decode($_REQUEST["ordinamento"], true);
						//se l'utente ha fatto un ordinamento, aggiorna la session
						if((isset($lista_ordinamento)) && ($lista_ordinamento != null)){
							//print_r($lista_ordinamento);
							$card_selezionate_utente = array();
							//inserisco la categoria alla sessione
							
							//
							foreach($lista_ordinamento as $chiave =>$val){ 
						    //$_SESSION["ordinamento_utente"]["$chiave"] = array();
								
						    foreach($val as $valore){ 
				    	//$aggiungi_occorrenza = $this->modello->aggiungi_occorrenza($valore, $chiave, $numstudio);
						    	array_push($card_selezionate_utente, str_replace('_', ' ', $valore));
				      
    							} 

    							
    							}
								//$lista_ordinamento["$chiave"] = str_replace($vecchia, $modificata, $lista_ordinamento["$chiave"]);
								$lista_ordinamento = change_key( $lista_ordinamento, $vecchia, $modificata );
    							//aggiorno le card rimanenti
    							$_SESSION["card"] = array_diff($_SESSION["card"], $card_selezionate_utente);
    							//inizializzo variabile sessione ordinamento
    							//$_SESSION["ordinamento_utente"] = 1;
    							//modifico session categorie
    							//print_r($lista_ordinamento);
									$key = array_search($vecchia, $_SESSION["categorie"]);
									$_SESSION["categorie"][$key] = str_replace($vecchia, $modificata, $_SESSION["categorie"][$key]);
									//	print_r($_SESSION["categorie"]);
	    				//$_SESSION["ordinamento_utente"] = [$chiaves];
	    				include ROOT . DS . "template" . DS . "usercategory.php";
							exit();
							} 
							
						//altrimenti modifico solo la categoria alla sessione
						
						foreach ($_SESSION["categorie"] as $value) {
							$value = str_replace($vecchia, $modificata, $value);
						}
						
						
						//print_r($_SESSION["categorie"]);

						include ROOT . DS . "template" . DS . "usercategory.php";
						exit();
					}




					$lista_card = $this->modello->recupera_card_seq($numstudio);
					$lista_categorie = array();

					$_SESSION["card"] = $lista_card;
					$_SESSION["categorie"] = $lista_categorie;


					include ROOT . DS . "template" . DS . "useropencard.php";


						}


					if($type == "closed"){


						$lista_categorie = $this->modello->recupera_categorie($numstudio);
						$lista_card = $this->modello->recupera_card($numstudio);
						include ROOT . DS . "template" . DS . "userclosedcard.php";

						}
					if($type == "tree"){

						$lista_categorie = $this->modello->recupera_categorie($numstudio);
						$lista_schema = $this->modello->recupera_schema($numstudio);
						$lista_task = $this->modello->recupera_task($numstudio);
						include ROOT . DS . "template" . DS . "usertree.php";

						}
				
				}
				else
				 {

					include ROOT . DS . "template" . DS . "studyclosed.php";
			}


			
		}
		elseif ($comando == "fatto") {
			$numstudio = $_REQUEST["studio"];
			if (isset($_REQUEST["ordinamento"])){
				$ordinamento = $_REQUEST["ordinamento"];
				$or = json_decode($ordinamento, true);
				//print_r($or);
				//$or = replaceKeys($or);

			/*	foreach($lista_ordinamento as $key => $val){
					$nkey = str_replace("_tot", "", $key);
					$lista_ordinamento[$nkey] = $lista_ordinamento[$key];
    					
   					 unset($lista_ordinamento[$key]);
					}
*/
				



				foreach($or as $chiave =>$val){ 
				    
				  
				    foreach($val as $valore){ 
				    	$valore = str_replace('_', ' ', $valore);
				    	$categoria = escapeLineInverse($chiave);
				    	$aggiungi_occorrenza = $this->modello->aggiungi_occorrenza($valore, $categoria, $numstudio);

				      
    					} 
    				
					} 
					
				}
				
				$aggiorna_partecipanti = $this->modello->aggiungi_n_partecipanti($numstudio);
				//  distruggi tabella utente
				$elimina_tab_utente = $this->modello->elimina_tab_provvisoria($_SESSION["user"]);
				session_unset();
				session_destroy();	
				include ROOT . DS . "template" . DS . "thanks.php";	
				//	echo $_SESSION["user"];
				}
				elseif ($comando == "fattot") {
					$numstudio = $_REQUEST["studio"];
					if (isset($_REQUEST["ordinamento"])){
					$ordinamento = $_REQUEST["ordinamento"];
					$or = json_decode($ordinamento, true);
					//print_r($or);
					
					foreach($or as $chiave =>$val){ 
					    
					    foreach($val as $valore){ 
					    	$categoria = escapeLineInverse($chiave);
					    	$aggiungi_occorrenza = $this->modello->aggiungi_occorrenza($valore, $categoria, $numstudio);

					        
					    } 
					  
					} 
				
				}
				
				$aggiorna_partecipanti = $this->modello->aggiungi_n_partecipanti($numstudio);
				//  distruggi tabella utente
				$elimina_tab_utente = $this->modello->elimina_tab_provvisoria($_SESSION["user"]);
				session_unset();
				session_destroy();
				include ROOT . DS . "template" . DS . "thanks.php";
				//	echo $_SESSION["user"];
		}
		elseif ($comando == "fattoto") {
			$numstudio = $_REQUEST["studio"];
			if (isset($_REQUEST["task"])){
			$risposte_task= json_decode($_REQUEST["task"], true);
			$lista_task = $this->modello->recupera_task($numstudio);
			$num_task = count($lista_task);


			//echo $num_task;
			$i=0;
			foreach($lista_task as $chiave =>$val){ 
				if($val->sottocat == $risposte_task[$i]){
					$aggiungi_ok = $this->modello->aggiungi_task_ok($numstudio, $val->idtaskn);
					$aggiungi_user_task = $this->modello->aggiungi_task_user_one($numstudio, $val->idtaskn, $val->taskn, $val->categ, $val->sottocat);
					
				}else
				{
					$aggiungi_wrong = $this->modello->aggiungi_task_wrong($numstudio, $val->idtaskn);
					$aggiungi_user_task = $this->modello->aggiungi_task_user_zero($numstudio, $val->idtaskn, $val->taskn, $risposte_task[$i]);
					
				}
				$i = $i + 1;
				}
			
			}
			$aggiorna_partecipanti = $this->modello->aggiungi_n_partecipanti($numstudio);
			
			//  distruggi tabella utente
			$elimina_tab_utente = $this->modello->elimina_tab_provvisoria($_SESSION["user"]);
			include ROOT . DS . "template" . DS . "thankstree.php";
			session_unset();
			session_destroy();
			//	echo $_SESSION["user"];
			}
			}else {
				include ROOT . DS . "template" . DS . "studyclosed.php";
			}
		}
}